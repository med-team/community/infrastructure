#!/bin/bash

# Copyright (C) Steffen Moeller <moeller@debian.org>

set -e

# To ensure that we all understand each other's error messages
# on the mailing lists
export LANG=C

d=`dirname $0`


if [ ! -r $d/debian-med.conf ]; then
  echo "Falling back to debian-med.conf.template"
  cp $d/debian-med.conf.template $d/debian-med.conf
fi

. $d/debian-med.conf

if [ -z "$country" ]; then
  c=`hostname --long|tr "." "\n" | tail -1 | tr -d '\n'`
  if [ "edu" == "$c" -o "com" == "$c" ]; then
    $country=".us"
  elif echo -n "$c" |egrep '^..$'; then
    echo "Assigning country and Debian mirror to '$c'"
    $country=".$c"
  fi
else
  country=".$country"
fi

command="lh_config --binary-images usb-hdd"
#command="$command --sections main contrib non-free"
command="$command --packages-list kde-core "
command="$command --mirror-bootstrap=http://ftp$country.debian.org/debian"

if [ -z "$distribution" ]; then
  echo "distribution not explicitly set."
else
  command="$command --distribution $distribution"
fi
command="$command --losetup losetup.orig"

#if [ -z "$architecture" ]; then
#  echo "architecture not explicitly set."
#else
#  command="$command --architecture $architecture"
#fi

if ! $command; then
  echo "lh_config was not successful."
  exit -1
fi

if [ ! -d config/chroot_local-packageslists ]; then
  echo "Could not configure the list of packages at 'config/chroot_local-packageslists'."
  exit -1
fi
echo "localepurge $main $contrib $nonfree $experimental $extraneous" | tr " " "\n" > config/chroot_local-packageslists/mypackages

if [ -n "$unofficialServers" ]; then
  rm -rf config/chroot_sources/sources.list.bootstrap
  rm -rf config/chroot_sources/sources.list.binary
  for server in `echo $unofficialServers|tr "," "\n"`
  do
     l="deb $server ./"
     #echo $l >> config/chroot_sources/sources.list.bootstrap
     #echo $l >> config/chroot_sources/sources.list.binary
  done
fi


# Putting shell scripts at the right position to further amend the setup

if [ -z "$hooks" ]; then
  echo "No hooks defined, skipping."
else
  if [ ! -d "$d/hooks" ]; then
    echo "Could not find directiory '$d/hooks'."
    exit -1
  fi

  dd="$d/config/chroot_local-hooks"
  if [ ! -d "$dd" ]; then
    echo "Could not find destination directory for hooks that is expected at '$dd'."
    exit -1
  fi

  for h in `echo $hooks| tr " " "\n"`; do
    if [ ! -r "$d/hooks/$h" ]; then
      echo "Hook '$h' not found in '$d/hooks'."
      exit -1
    fi
    cp $d/hooks/$h $dd
  done
fi


# Continuation of the build process

if lh_build; then
   echo lh_build completed successfully
else
   echo lh_build did not complete nicely
   exit -1
fi

if lh_binary; then
	echo lh_binary completed successfully
else
	cat <<EOINFO
lh_binary did not complete nicely. If the error message is not sufficiently informative, then please check:
  * loop-aes-modules-2.6-amd64 is installed and 
    functional with your current kernel.
Do a 'lh_clean --binary' and run `basename $0|tr -d '\n'` again.
EOINFO
		exit -1
fi

