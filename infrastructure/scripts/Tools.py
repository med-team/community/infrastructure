#!/usr/bin/python -Ot

#
# This Python script is:
#  (C) 2007, David Paleino <d.paleino@gmail.com>
#
# It is licensed under the terms of GNU General Public License (GPL) v2 or later.
#

import os
import re

base = "/var/lib/gforge/chroot/home/groups/debian-med"
tasks_repos = "svn://svn.debian.org/svn/cdd/projects/med/trunk/debian-med/tasks/"

tasks = "%s/data/tasks" % base

#
# This is used to parse Task files.
#

packages = []

def inner_parseTask(file, doSeparate):
	global packages

	if doSeparate:
		name = file.name.split("/")[-1]
		if not type(packages) == dict:
			packages = {}
		if name not in packages:
			packages[name] = []
		stepone = {}
		if name not in stepone:
			stepone[name] = []
		steptwo = {}
		if name not in steptwo:
			steptwo[name] = []
	else:
		stepone = []
		steptwo = []

	pattern = re.compile(r"Depends:")
	continued = False
	for line in file.readlines():
		match = pattern.match(line)
		if match and not continued:
			if doSeparate:
				stepone[name].append(line.replace("Depends:", "").strip().split(","))
			else:
				stepone.append(line.replace("Depends:", "").strip().split(","))
			if line.strip().endswith("\\") == True:
				continued = True
				continue
		elif continued == True:
			if doSeparate:
				stepone[name].append(line.strip().split(","))
			else:
				stepone.append(line.strip().split(","))
			if line.strip().endswith("\\") == True:
				continued = True
			else:
				continued = False

			continue

	if doSeparate:
		for item in stepone[name]:
			if type(item) == str:
				steptwo[name].append(item.split("|"))
			elif type(item) == list:
				for inner in item:
					steptwo[name].append(inner.split("|"))

		for item in steptwo[name]:
			if type(item) == str and item != "":
				packages[name].append(item.strip())
			elif type(item) == list:
				for inner in item:
					if inner != "":
						packages[name].append(inner.strip())
		steptwo[name] = packages[name]
		try:
			packages[name] = list(set(steptwo[name].remove('\\')))
		except:
			packages[name] = list(set(steptwo[name]))

		packages[name].sort()
	else:
		for item in stepone:
			if type(item) == str:
				steptwo.append(item.split("|"))
			elif type(item) == list:
				for inner in item:
					steptwo.append(inner.split("|"))

		for item in steptwo:
			if type(item) == str and item != "":
				packages.append(item.strip())
			elif type(item) == list:
				for inner in item:
					if inner != "":
						packages.append(inner.strip())

		steptwo = packages
		try:
			packages = list(set(steptwo.remove('\\')))
		except:
			packages = list(set(steptwo))

		packages.sort()

def parseTasks(name = None, doSeparate = False):
	#
	# Let's get our task files
	#

	os.system("svn co %s %s >> /dev/null" % (tasks_repos, tasks))

	if not name:
		for file in os.listdir(tasks):
			if os.path.isfile("%s/%s" % (tasks, file)):
				f = open("%s/%s" % (tasks, file))
				inner_parseTask(f, doSeparate)
				f.close()
	else:
		if os.path.isfile("%s/%s" % (tasks, name)):
			f = open("%s/%s" % (tasks, name))
			inner_parseTask(f, doSeparate)
			f.close()

	return packages

def parseTasksNonOff(task = None):
	pkg_desc = {}

	regex = r"""Depends: (?P<package>.*)
Homepage: (?P<homepage>.*)
Pkg-URL: (?P<deburl>.*)
License: (?P<license>.*)
Pkg-Description: (?P<short>.*)
(?P<long>(^ .*$\n)+\n?)"""

	p = re.compile(regex, re.MULTILINE)
	if task:
		try:
			if os.path.isfile("%s/%s" % (tasks, task)):
				f = open("%s/%s" % (tasks, task))
				tuples = p.findall(f.read())
				for group in tuples:
					(package, homepage, deburl, license, short, long, foo) = group
					pkg_desc[package] = {}
					pkg_desc[package]['Homepage'] = homepage
					pkg_desc[package]['Pkg-URL'] = deburl
					pkg_desc[package]['License'] = license
					pkg_desc[package]['ShortDesc'] = short
					pkg_desc[package]['LongDesc'] = long.replace("\n .\n", "<br /><br />").replace("\n", "")

				f.close()
		except:
			# Check all task files if the specified one
			# does not exist.
			parseTasksNonOff(None)
	else:
		for file in os.listdir(tasks):
			if os.path.isfile("%s/%s" % (tasks, file)):
				f = open("%s/%s" % (tasks, file))
				tuples = p.findall(f.read())
				for group in tuples:
					(package, homepage, deburl, license, short, long, foo) = group
					if file not in pkg_desc:
						pkg_desc[file] = []
					tmp = {}
					tmp['Package'] = package
					tmp['Homepage'] = homepage
					tmp['Pkg-URL'] = deburl
					tmp['License'] = license
					tmp['ShortDesc'] = short
					tmp['LongDesc'] = long.replace("\n .\n", "<br /><br />").replace("\n", "")

					pkg_desc[file].append(tmp)

				f.close()

	return pkg_desc

def parseTasksUnavail(task = None):
	pkg_desc = {}

	regex = r"""Depends:(?P<package>.*)
Homepage:(?P<homepage>.*)
Responsible:(?P<responsible>.*)
License:(?P<license>.*)
WNPP:(?P<wnpp>.*)
Pkg-Description:(?P<short>.*)
(?P<long>(^ .*$\n)+\n?)"""

	p = re.compile(regex, re.MULTILINE)
	if task:
		try:
			if os.path.isfile("%s/%s" % (tasks, task)):
				f = open("%s/%s" % (tasks, task))
				tuples = p.findall(f.read())
				for group in tuples:
					(package, homepage, responsible, license, wnpp, short, long, foo) = group
					pkg_desc[package] = {}
					pkg_desc[package]['Homepage'] = homepage.strip()
					pkg_desc[package]['Responsible'] = responsible.strip()
					pkg_desc[package]['License'] = license.strip()
					pkg_desc[package]['WNPP'] = wnpp.strip()
					pkg_desc[package]['ShortDesc'] = short.strip()
					pkg_desc[package]['LongDesc'] = long.replace("\n .\n", "<br /><br />").replace("\n", "")

				f.close()
		except:
			# Check all task files if the specified one
			# does not exist.
			parseTasksNonOff(None)
	else:
		for file in os.listdir(tasks):
			if os.path.isfile("%s/%s" % (tasks, file)):
				f = open("%s/%s" % (tasks, file))
				tuples = p.findall(f.read())
				for group in tuples:
					(package, homepage, responsible, license, wnpp, short, long, foo) = group
					if file not in pkg_desc:
						pkg_desc[file] = []
					tmp = {}
					tmp['Package'] = package
					tmp['Homepage'] = homepage.strip()
					tmp['Responsible'] = responsible.strip()
					tmp['License'] = license.strip()
					tmp['WNPP'] = wnpp.strip()
					tmp['ShortDesc'] = short.strip()
					tmp['LongDesc'] = long.replace("\n .\n", "<br /><br />").replace("\n", "")

					pkg_desc[file].append(tmp)

				f.close()

	return pkg_desc

def parseTaskDetails(file = None):
	det = {}

	regex = r"""Task: (?P<task>.*)
Description: (?P<short>.*)
(?P<long>(^ .*$\n)+\n?)"""

	p = re.compile(regex, re.MULTILINE)
	if file:
		try:
			if os.path.isfile("%s/%s" % (tasks, file)):
				f = open("%s/%s" % (tasks, file))
				m = p.search(f.read())
				if m:
					det['Task'] = m.group("task")
					det['ShortDesc'] = m.group("short")
					det['LongDesc'] = m.group("long").replace("\n .\n", "<br /><br />").replace("\n", "")

				f.close()
		except:
			# Check all task files if the specified one
			# does not exist.
			parseTasksNonOff(None)
	else:
		for file in os.listdir(tasks):
			if os.path.isfile("%s/%s" % (tasks, file)):
				f = open("%s/%s" % (tasks, file))
				m = p.search(f.read())
				if m:
					if file not in det:
						det[file] = {}
					det[file]['Task'] = m.group("task")
					det[file]['ShortDesc'] = m.group("short")
					det[file]['LongDesc'] = m.group("long")

				f.close()

	return det

def grep(search, file):
	try:
		f = open(file, "r")
	except:
		return False

	try:
		p = re.compile(r"%s" % re.escape(search))
	except:
		print "ERROR:"
		print "    searching %s in %s" % (search, file)
		print "    escaped search: %s" % re.escape(search)
		return False

	for line in f.readlines():
		m = p.match(line)
		if m:
			return True

	f.close()
	return False
