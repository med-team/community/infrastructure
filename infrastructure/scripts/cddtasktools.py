#!/usr/bin/python
# Copyright 2008: Andreas Tille <tille@debian.org>
# License: GPL

# CDD Meta packages are listing a set of Dependencies
# These might be fullfilled by the Debian package
# set or not.
#
# This interface provides some classes that contains
# all available information about this Dependency ,
# like whether it is an official package or not,
# in which distribution it is contained
# or if it is not contained it obtains information
# from tasks file about home page, license, WNPP etc.

from sys import stderr, exit
import os
import urllib
import StringIO
import gzip
import bz2
import re

from debian_bundle import deb822

BASEURL  = 'http://ftp.debian.org/'
#SVNHOST  = 'svn+ssh://svn.debian.org'
SVNHOST  = 'svn://svn.debian.org'
REPOS    = { 'debian-med'     : SVNHOST+"/svn/cdd/projects/med/trunk/debian-med/tasks/",
             'debian-edu'     : SVNHOST+"/svn/debian-edu/trunk/src/debian-edu/tasks/",
             'debian-gis'     : SVNHOST+"/pkg-grass/packages/debian-gis/tasks/",
             'debian-junior' : SVNHOST+"/svn/cdd/projects/junior/trunk/debian-junior/tasks/",
             'debian-science' : SVNHOST+"/svn/cdd/projects/science/trunk/debian-science/tasks/",
           }
HTMLBASE = "/var/lib/gforge/chroot/home/groups"
DATADIR  = { 'debian-med'     : HTMLBASE+"/debian-med/data/",
             'debian-edu'     : HTMLBASE+"/cdd/data/edu/",
             'debian-gis'     : HTMLBASE+"/cdd/data/gis/",
             'debian-junior'  : HTMLBASE+"/cdd/data/junior/",
             'debian-science' : HTMLBASE+"/cdd/data/science/",
           }
KEYSTOIGNORE = ( 'Architecture', 'Leaf', 'NeedConfig', 'Note', 'Section' )
GLOBALCACHE  = "/var/cache/cdd/"
DDTPURL = "http://ddtp.debian.net/debian/dists/"
DDTPDIR = "/i18n/Translation-"
DDTPLISTS = ('etch', 'lenny', 'sid')

# COMPRESSIONEXTENSION='bz2'
COMPRESSIONEXTENSION='gz'

def InitTasksFiles(cddname):
    # Obtain tasks files from SVN of a CDD
    # cddname can be: debian-med, debian-edu, debian-science
    # In case you know another CDD that uses the meta package
    # technology make sure to include the location in the
    # REPOS dictionary
    #
    tasksdir = DATADIR[cddname] + 'tasks/'
    if not os.access(tasksdir, os.W_OK):
	try:
		os.makedirs(tasksdir)
	except:
		print >> stderr, "Unable to create data directory", tasksdir
    # Checkout/Update tasks from SVN
    if os.path.isdir(tasksdir+'/.svn'):
        os.system("svn up %s %s >> /dev/null" % (REPOS[cddname], tasksdir))
    else:
        os.system("mkdir -p %s" % (tasksdir))
        os.system("svn co %s %s >> /dev/null" % (REPOS[cddname], tasksdir))
    return tasksdir

def SplitDescription(description):
    # Split first line of Description value as short description
    
    lines = description.splitlines()
    ShortDesc = lines[0].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
    LongDesc  = ''
    pre       = 0
    for line in lines[1:]:
        # Replace paragraph separators by <br />
        if re.compile("^\s?\.\s*$").search(line):
            LongDesc += "<br />\n"
        else:
            # Add <pre> tag if indentation > 2 spaces
            if pre == 1 and re.compile("^\s[^\s]").search(line):
                LongDesc += "</pre>\n"
                pre    = 0 # end of pre
            elif pre == 0 and re.compile("^\s\s").search(line):
                pre = 1
                LongDesc += "<pre>\n"
            # This is to sanitize output for XHTML pages. --David
            # Leave out the first space (line[1:]) instead of strip() because strip
            # removes wanted indentation of pre statements
            line = line[1:].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;") + ' \n'
            LongDesc += re.sub('([fh]t?tp://[-./\w?=~;]+)', '<a href="\\1">\\1</a>', line)
    if pre == 1:
        LongDesc += "</pre>\n"
    return (ShortDesc, LongDesc)

class DependantPackage:
    # Hold information about a program that is in dependency list
    # The 

    def __init__(self, cddname=None, taskname=None):
        self.cddname        = cddname  # CDD that includes the package in dependency list
        self.taskname       = taskname # Task which includes the Dependency
        self.pkg            = None # Name of dependant package
        self.dep_strength   = None # Values: 'Depends', 'Recommends', 'Suggests', 'Ignore', 'Avoid'
        self.dists          = []   # Values: 'stable', 'testing', 'unstable', etc.
        self.component      = {}   # Values: 'main', 'contrib', 'non-free', 'experimental'

        self.why            = None # basically used as comment

        # The following keys will be mostly used for programs that
        # are not yet existing in Debian and will go to our todo list
        self.homepage       = '#'  # Homepage of program
        self.version        = None # Version of program
        self.responsible    = None # E-Mail address of issuer of ITP or some person
                                   # who volunteered to care for this program
        self.license        = None # License of program
        self.section        = None # Section of package in the Debian hierarchy
        self.filename       = None # Filename of package in the Debian pool
        self.wnpp           = None # WNPP bug number
        self.pkgShortDesc   = None # Prospective packages should have a description ...
        self.pkgLongDesc    = None # ... which could be copied to (or from if exists)
                                   # WNPP bug and finally can be used for packaging
        self.pkgURL         = None # URL of unofficial package

    # sort these objects according to the package name
    def __cmp__(self, other):
        # Comparing with None object has to return something reasonable
        if other == None:
            return -2
        # Sort according to package name
        return cmp(self.pkg, other.pkg)

class CddDependencies:
    # Provide a list of depencencies defined in Metapackages
    # This class concerns _all_ tasks of a CDD and is the most
    # complete source of information.  If only a single task
    # should be handled by a tool that uses cddtasktools
    # probably the class TaskDependencies (see below) is
    # your friend

    def __init__(self, cddname):

        # This Instance of the Available class contains all
        # information about packages that are avialable in Debian
        # See below for more information about Available class
        global available

        if cddname not in REPOS.keys():
            print >>stderr, "Unknown CDD."
            return None

        self.cddname         = cddname
        self.tasksdir        = InitTasksFiles(self.cddname)
        self.tasks           = {}
        self.tasknames       = []
        self.available       = available
        self.alldeps_in_main = []

    def GetTasknames(self):
        for task in os.listdir(self.tasksdir):
            if os.path.isfile("%s/%s" % (self.tasksdir, task)):
                self.tasknames.append(task)
        self.tasknames.sort()

    def GetAllDependencies(self):
        if self.tasknames == []:
            self.GetTasknames()
        for task in self.tasknames:
            td = TaskDependencies(self.cddname, task=task, tasksdir=self.tasksdir)
            td.GetTaskDependencies()
            self.tasks[task] = td

    def GetNamesOnlyDict(self, dependencytypes=()):
        # David Paleino needs for his web tools a dictionary
        # { taskname : [list of dependencies]}
        # This will be prepared here from the main
        # datastructure
        ret = {}
        if dependencytypes == ():
            # official:    A package with this name is found in the Debian repository
            # unofficial:  The tasks file contains a tag Pkg-URL for the package which is not None
            # prospective: At least a short and long description are attached to a package name which
            #              is not in the Debian pool and has no Pkg-URL for an unofficial package
            # unknown:     Any other package names that are listed as Dependencies but have
            #              incomplete information.  This usually happens when packages are not
            #              available in Debian any more (for instance this might happen if a
            #              name has changed)
            dependencytypes=('official', 'unofficial', 'prospective', 'unknown')
        for task in self.tasknames:
            tdeps = self.tasks[task]
            list = []
            for dep in dependencytypes:
                for tdep in tdeps.dependencies[dep]:
                    list.append(tdep.pkg)
            ret[task] = list
        return ret

    def GetListOfDepsForTask(self, task, dependencytypes=()):
        # David Paleino needs for his web tools a dictionary
        # [list of dependencies]
        # This will be prepared here from the main
        # datastructure
        ret = []
        if dependencytypes == ():
            dependencytypes=('official', 'unofficial', 'prospective', 'unknown')
        tdeps = self.tasks[task]
        for dep in dependencytypes:
            for tdep in tdeps.dependencies[dep]:
                ret.append(tdep.pkg)
        return ret

    def GetTaskDescDict(self):
        # David Paleino needs for his web tools a dictionary
        # { taskname : { 'Task'      : task
        #                'ShortDesc' : shortdesc
        #                'LongDesc'  : longdesc }
        # This will be prepared here from the main
        # datastructure
        ret = {}
        for task in self.tasknames:
            tdeps = self.tasks[task]
            tdict = {}
            tdict['Task']      = tdeps.taskPrintedName
            tdict['ShortDesc'] = tdeps.taskShortDesc
            tdict['LongDesc']  = tdeps.taskLongDesc
            ret[task] = tdict
        return ret

    def GetAllDependantPackagesOfCDD(self, dependencytypes=()):
        # David Paleino needs for his DDTP web tool a list of
        # all available Dependencies.
        # Here only those packages are returned that are in
        # Debian main, because there are no DDTP translations
        # for contrib and non-free available
        if self.alldeps_in_main != []:
            return self.alldeps_in_main
        if dependencytypes == ():
            # official:    A package with this name is found in the Debian repository
            # unofficial:  The tasks file contains a tag Pkg-URL for the package which is not None
            # prospective: At least a short and long description are attached to a package name which
            #              is not in the Debian pool and has no Pkg-URL for an unofficial package
            # unknown:     Any other package names that are listed as Dependencies but have
            #              incomplete information.  This usually happens when packages are not
            #              available in Debian any more (for instance this might happen if a
            #              name has changed)
            # Default is only official in this case because DDTP handles only
            # official packages
            dependencytypes=('official', )
        for task in self.tasknames:
            tdeps = self.tasks[task]
            for dep in dependencytypes:
                for tdep in tdeps.dependencies[dep]:
                    # add only packages in main, because others do not have DDTP translations
                    if tdep.component == 'main':
                        self.alldeps_in_main.append(tdep.pkg)
        self.alldeps_in_main.sort()
        return self.alldeps_in_main

    def GetTranslations(self, languages=()):
        # Get DDTP translations for the package dependencies in a CDD

        if languages == ():
            return

        # Make sure that cache directory for DDTP translations exists and is writable
	cachedir = GLOBALCACHE
	if not os.access(cachedir, os.W_OK):
    	    try:
    		os.mkdir(cachedir)
    	    except:
    		# If we have no access to global cache create a local one
    		cachedir = "cache"
		if not os.access(cachedir, os.W_OK):
    		    os.mkdir(cachedir)
        # Use sid as main source of DDTP information
        dist = 'sid'
        # Only main has DDTP translations
        component = 'main'

        pks2translate = self.GetAllDependantPackagesOfCDD()
        trans = {}
        for lang in languages:
            # f = urllib.urlopen(DDTPURL+dist+'/'+component+DDTPDIR+'/'+lang)
            cachefile = cachedir+'/'+dist+'_'+component+'_'+lang+'.'+COMPRESSIONEXTENSION
            (file,info) = urllib.urlretrieve(DDTPURL+dist+'/'+component+DDTPDIR+lang+'.'+COMPRESSIONEXTENSION,cachefile)
            if COMPRESSIONEXTENSION == 'gz':
                g = gzip.GzipFile(file)
            elif COMPRESSIONEXTENSION == 'bz2':
                g = bz2.BZ2File(file)
            else:
                print >>stderr, "Unknown compression extension " + COMPRESSIONEXTENSION
                return exit(-1)

            trans[lang] = {}
            for stanza in deb822.Sources.iter_paragraphs(g, shared_storage=False):
                pkg = stanza['package']
                if pkg not in pks2translate:
                    continue
                trans[lang][pkg] = SplitDescription(stanza['description-'+lang])
        return trans


class TaskDependencies:
    # List of depencencies defined in one Metapackage
    def __init__(self, cddname, task, tasksdir=None):
        if cddname not in REPOS.keys():
            print >>stderr, "Unknown CDD."
            return None

        self.cddname  = cddname
        if tasksdir != None:
            self.tasksdir = tasksdir
        else:
            self.tasksdir = InitTasksFiles(self.cddname)
        self.taskfile = self.tasksdir+'/'+task
        if os.path.isfile(self.taskfile):
            self.task = task
        else:
            print >>stderr, "No such task file %s." % self.taskfile
            return None

        # Dictionary with dependencytype as key and list of DependantPackage
        # instances
        self.dependencies = { 'official'    : [],
                              'unofficial'  : [],
                              'prospective' : [],
                              'unknown'     : []
                            }
        self.available = available

        # Main information for a task
        self.taskPrintedName = None
        self.taskShortDesc   = None
        self.taskLongDesc    = None

        # If a CDD just bases on the meta package of an other CDD (this is the
        # case in Debian Science which bases on med-bio for biology and gis-workstation
        # for geography it makes no sense to build an own sentinel page but read
        # meta package information of other meta packages and include the content
        # of these while enabling to add further Dependencies as well
        #
        # metadepends should be a SVN URL
        #
        # This is NOT YET implemented
        self.metadepends     = None

    def GetTaskDependencies(self):
        # First obtain information about Packages
        # available in Debian
        self.available.GetPackageNames()

        # These keys might contain more than one item that
        # has to be separated to detect the dependency
        dep_strength_keys = [ 'Depends', 'Recommends', 'Suggests', 'Ignore', 'Avoid' ] 

        f = file(self.taskfile)
        for stanza in deb822.Sources.iter_paragraphs(f):
            # Why and Responsible can be valid for more than one dependency
            # Store them in strings and use them for all Dependent Package objects
            why         = None
            responsible = None
            dep         = None
            for key in stanza:
                if key == 'Task':
                    self.taskPrintedName = stanza['task']
                    continue
                if key == 'Description':
                    (self.taskShortDesc, self.taskLongDesc) = SplitDescription(stanza['description'])
                    continue
                if key == 'Meta-Depends':
                    self.metadepends = stanza['meta-depends']
                if key == 'Why':
                    why = stanza['why']
                    continue
                if key == 'Responsible':
                    responsible = stanza['responsible'].strip()
                    if responsible != '':
                        responsible = re.sub('\s*(.+)\s+<(.+@.+)>\s*', '<a href="mailto:\\2">\\1</a>', responsible)
                        dep.responsible = responsible
                    continue

                if key in dep_strength_keys:
                    # Hack: Debian Edu tasks files are using '\' at EOL which is broken
                    #       in RFC 822 files, but cdd-gen-control from cdd-dev 0.5.0 relies
                    #       on this.  So remove this stuff here for the Moment
                    dependencies = re.sub('\\\\\n\s+', '', stanza[key])

                    # turn alternatives ('|') into real depends for this purpose
                    # because we are finally interested in all alternatives
                    dependencies = dependencies.replace('|',',').split(',')
                    # Collect all dependencies in one line first,
                    # create an object for each later
                    deps_in_one_line = []
                    for dependency in dependencies:
                        deps_in_one_line.append(dependency.strip())

                    for dep_in_line in deps_in_one_line:
                        # If there are more than one dependencies in one line
                        # just put the current one into the right list of dependencies
                        # before initiating the next instance
                        if dep != None:
                            # In case we should ignore the package do not add it to Dependencies
                            if dep.dep_strength != 'Ignore' and dep.dep_strength != 'Avoid':
                                self.dependencies[self._FindDependencyType(dep)].append(dep)
                            else:
                                # print "Package %s actively ignored / avoided." % dep.pkg
                                pass
                        dep = DependantPackage(self.cddname, self.task)
                        # Store the comments in case they might be usefull for later applications
                        dep.why            = why
                        dep.responsible    = responsible
                        dep.dep_strength   = key
                        dep.pkg            = dep_in_line
                        dep.dists.append(self.available.dist)
                        # Find the component the Dependency might be in
                        for component in self.available.components:
                            if dep.pkg in self.available.packages[component].keys():
                                dep.component    = component
                                if component == 'main':
                                    dep.license = 'DFSG free'
                                elif component == 'contrib':
                                    dep.license = 'DFSG free, but needs non-free components'
                                elif component == 'non-free':
                                    dep.license = 'non-free'
                                else:
                                    dep.license = 'unknown'
                                dep.pkgShortDesc = self.available.packages[component][dep.pkg].pkgShortDesc
                                dep.pkgLongDesc  = self.available.packages[component][dep.pkg].pkgLongDesc
                                dep.homepage     = self.available.packages[component][dep.pkg].homepage
                                dep.version      = self.available.packages[component][dep.pkg].version
                                dep.responsible  = self.available.packages[component][dep.pkg].responsible
                                # TODO: Assuming 'unstable' is wrong --> handle list of dists
                                dep.pkgURL       = 'http://packages.debian.org/' + 'unstable/' + \
                                                   self.available.packages[component][dep.pkg].section + \
                                                   '/' + dep.pkg
                                dep.filename     = self.available.packages[component][dep.pkg].filename
                                break # The same package should be only in one component
                                      # At least I currently see no reason for having a
                                      # package with the same name in main and contrib
                                      # at the same time for instance
                    continue
                # The following keys will be mostly used for programs that
                # are not yet existing in Debian and will go to our todo list
                if key.lower() == 'homepage':
                    if dep != None:
                        dep.homepage = stanza['homepage']
                    else:
                        print >>stderr, "Dep not initiated before Homepage %s -> something is wrong." \
                              % stanza['homepage']
                elif key == 'section':
                    if dep != None:
                        dep.section  = stanza['section']
                    else:
                        print >>stderr, "Dep not initiated before Section %s -> something is wrong." \
                              % stanza['license']
                elif key == 'License':
                    if dep != None:
                        dep.license  = stanza['license']
                    else:
                        print >>stderr, "Dep not initiated before License %s -> something is wrong." \
                              % stanza['license']
                elif key == 'WNPP':
                    if dep != None:
                        dep.wnpp     = stanza['wnpp']
                    else:
                        print >>stderr, "Dep not initiated before WNPP %s -> something is wrong." \
                              % stanza['wnpp']
                elif key == 'Pkg-URL':
                    if dep != None:
                	# Escape '&' in URLs with %26 (Trick stolen by pasting the URL into a search engine ;-))
                        dep.pkgURL   = stanza['pkg-url'].replace("&", "%26")
                    else:
                        print >>stderr, "Dep not initiated before Pkg-URL %s -> something is wrong." \
                              % stanza['pkg-url']
                elif key == 'Pkg-Description':
                    if dep == None:
                        print >>stderr, "Dep not initiated before Pkg-Description %s -> something is wrong." \
                              % stanza['pkg-description'].splitlines()[0]
                    else:
                        (dep.pkgShortDesc, dep.pkgLongDesc) = SplitDescription(stanza['pkg-description'])
                else:
            	    if key not in KEYSTOIGNORE:
                        # Also ignore keys starting with X[A-Z]-
                        if not re.compile("^X[A-Z]*-").search(key):
                            print "Unknown key '%s': %s" % (key, stanza[key])
            if dep != None:
                if dep.dep_strength != 'Ignore' and dep.dep_strength != 'Avoid':
                    self.dependencies[self._FindDependencyType(dep)].append(dep)
                else:
                    # print "Package %s actively ignored / avoided." % dep.pkg
                    pass
                
        f.close()

        for dependency in self.dependencies.keys():
            self.dependencies[dependency].sort()

    def _FindDependencyType(self, dep):
        # Return the name of the Dependencytype to append the Dependency to the right list
        if dep.component != {}:
            return 'official'
        if dep.pkgURL != None:
            return 'unofficial'
        if dep.pkgShortDesc != None and dep.pkgLongDesc != None:
            return 'prospective'
        return 'unknown'


class Available:
    # Information about available packages
    #
    # Usage example:
    #    available = Available(                     # Initialize instance
    #                          dist='testing',      # (default='unstable')
    #                          components=('main'), # Regard only main, default: main, contrib, non-free
    #                          source=1             # Use source package names, default: use binaries
    #                          arch='sparc'         # (default='i386')
    #                         )
    # 
    #    available.GetPackageNames() # Actually parse the Packages files to obtain needed information
    #                                # This has to be done at least once.  It is verified that the effort
    #                                # to obtain package information is not done twice per run


    def __init__(self, dist=None, components=(), source=None, arch=None):
        self.source = 'Packages.'+COMPRESSIONEXTENSION
        if source != None:
            self.source = 'Sources.'+COMPRESSIONEXTENSION
        self.binary = 'source'
        if source == None:
            if arch == None:
                # use arch=i386 as default because it contains most packages
                self.binary = 'binary-i386'
            else:
                self.binary = 'binary-' + arch
        self.dist = 'unstable'
        if dist != None:
            self.dist = dist
        self.components = ('main', 'contrib', 'non-free')
        if components != ():
            self.components = components
        # The dictionary packages contains the component as key
        # The values are dictionaries holding package names as key
        # and a DependantPackage object as values
        self.packages = {}
        for component in self.components:
            self.packages[component] = {}

    def GetPackageNames(self):
        # Fetch Packages / Sources file and get list of package names out of it

        # Check whether package names are just known
        for component in self.packages:
            if self.packages[component] != {}:
                # Just got the Package names because at least one component
                # has non zero elements
                return

	cachedir = GLOBALCACHE
	#if ! -d cachedir :
	if not os.access(cachedir, os.W_OK):
    	    try:
    		os.mkdir(cachedir)
    	    except:
    		# If we have no access to global cache create a local one
    		cachedir = "cache"
		if not os.access(cachedir, os.W_OK):
    		    os.mkdir(cachedir)
        for component in self.components:
            # f = urllib.urlopen(BASEURL+'/dists/'+self.dist+'/'+component+'/'+self.binary+'/'+self.source)
            cachefile = cachedir+'/'+self.dist+'_'+component+'_'+self.binary+'_'+self.source
            (file,info) = urllib.urlretrieve(BASEURL+'/dists/'+self.dist+'/'+component+'/'+self.binary+'/'+self.source, cachefile)
            if COMPRESSIONEXTENSION == 'gz':
                g = gzip.GzipFile(file)
            elif COMPRESSIONEXTENSION == 'bz2':
                g = bz2.BZ2File(file)
            else:
                print >>stderr, "Unknown compression extension " + COMPRESSIONEXTENSION
                return exit(-1)
                
            for stanza in deb822.Sources.iter_paragraphs(g, shared_storage=False):
                deppkg = DependantPackage()
                (deppkg.pkgShortDesc, deppkg.pkgLongDesc) = SplitDescription(stanza['description'])
                try:
                    deppkg.homepage = stanza['homepage']
                except KeyError:
                    deppkg.homepage = '#' # Not every package has a homepage tag
                except:
                    deppkg.homepage = '.' # Something else in case unexpected things happen
                deppkg.version      = stanza['version'].split('-')[0]
                deppkg.section      = stanza['section']
                deppkg.responsible  = re.sub('\s*(.+)\s+<(.+@.+)>\s*', '<a href="mailto:\\2">\\1</a>', \
                                             stanza['maintainer'])
                deppkg.filename     = BASEURL+stanza['filename']
                self.packages[component][stanza['package']] = deppkg
            g.close()

available = Available()

