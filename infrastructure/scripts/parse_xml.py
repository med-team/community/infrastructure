#!/usr/bin/python

#~ Copyright 2007 David Paleino <d.paleino@gmail.com>
#~ 
#~ This program is free software; you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation; either version 3 of the License, or
#~ (at your option) any later version.
#~ 
#~ This program is distributed in the hope that it will be useful,
#~ but WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#~ GNU General Public License for more details.
#~ 
#~ You should have received a copy of the GNU General Public License
#~ along with this program; if not, write to the Free Software
#~ Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#~ MA 02110-1301, USA.

#~ This code heavily uses code from the CIA.vc project (http://cia.vc)
#~  by Micah Dowty <micah@navi.cx>
#~ The code of CIA.vc is free software, it is available under SVN
#~  at http://cia-vc.googlecode.com/svn/trunk/

from Nouvelle.Serial import Serializer
from LibCIA import Formatters
from LibCIA.Message import Message
from LibCIA.Message import FormatterArgs
from LibCIA.Web import RegexTransform

# This has been customized
#  -- David
import Commit

import getopt
import sys
import cgi
import cgitb; cgitb.enable()

### CIA.vc's /apps/stats/browser.py (adapted by David Paleino)

def to_html():
	"""Format any message as XHTML, using the LibCIA formatters and
	Nouvelle serializer. This is used as a fallback for non-commit
	messages.
	"""
	return serializer.render(formatter.formatMessage(msg))

def format_log():
	return serializer.render(commit.component_log(None, FormatterArgs(msg)))

def format_files():
	return serializer.render(commit.component_files(None, FormatterArgs(msg)))

def format_headers():
	return serializer.render(commit.component_headers(None, FormatterArgs(msg)))

###

serializer = Serializer()
msg = Message(open("message.xml").read())
formatter = Formatters.getFactory().findMedium('xhtml', msg)
commit = Commit.CommitToXHTMLLong()
hyperlinker = RegexTransform.AutoHyperlink()

try:
	(opts, args) = getopt.getopt(sys.argv[1:], '')
except:
	pass

#print "Content-Type: application/xhtml+xml"
print "Content-Type: text/html"
print
print """<html>
<head>
<title>SVN Commit Details</title>
<link rel="stylesheet" href="style.css" />
</head>
<body>
<h1>%s</h1>
%s<hr />
%s<hr />
%s</body>""" % (args, format_headers(), format_log(), format_files())
