# vim:ts=4:sw=4:et:ai:sts=4
# $Id: Archive.pm 13820 2008-01-29 06:11:41Z tincho-guest $
#
# Module for retrieving data from the Debian archive, it reads Source.gz files,
# and also downloads package lists from the NEW and INCOMING queues.
#
# Copyright gregor herrmann <gregor+debian@comodo.priv.at>, 2007
# Copyright Damyan Ivanov <dmn@debian.org>, 2007
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Released under the terms of the GNU GPL 2

package DebianQA::Archive;
use strict;
use warnings;

our @ISA = "Exporter";
our @EXPORT = qw(deb_download deb_get deb_get_consolidated);

use DebianQA::Cache;
use DebianQA::Common;
use DebianQA::Config '%CFG';
use DebianQA::Svn;
use DebianQA::DebVersions;
use Fcntl qw(:seek);
use LWP::UserAgent;
#use IO::Uncompress::Gunzip; # Only in lenny
use Compress::Zlib ();
use HTML::TableExtract;

my $ua = new LWP::UserAgent;
$ua->timeout(10);
$ua->env_proxy;

# Module for extracting source package listings from the Debian archive.
# * If $force, current cache is ignored.
#
# Re-generates and returns the cache of consolidated versions (key "archive"),
# which is keyed on package name and contains:
#  {
#     most_recent => $most_recent_version,
#     testing => $version_in_testing,
#     ....
#  }
sub deb_download {
    my $force = shift;
    my @list = split(/\s*,\s*/, $CFG{archive}{suites});
    my @ttls = split(/\s*,\s*/, $CFG{archive}{suites_ttl});
    my %ttl = map({ $list[$_] => $ttls[$_] } (0..$#list));

    if($CFG{archive}{new_url}) {
        push @list, "new";
        $ttl{new} = $CFG{archive}{new_ttl} || 60;
    }
    if($CFG{archive}{incoming_url}) {
        push @list, "incoming";
        $ttl{incoming} = $CFG{archive}{incoming_ttl} || 60;
    }
    my $data = {};
    unless($force) {
        $data = read_cache("archive", "", 0);
    }
    my $modified;
    foreach my $src (@list) {
        # I use find_stamp incorrectly on purpose: so each key acts as a root
        if($force or ! $data->{$src}
                or $ttl{$src} * 60 < time - find_stamp($data->{$src}, "")) {
            info("$src is stale, getting new version") unless($force);
            my $d;
            if($src eq "new") {
                $d = get_new();
            } elsif($src eq "incoming") {
                $d = get_incoming();
            } else {
                $d = get_sources($src);
            }
            if($d) {
                update_cache("archive", $d, $src, 1, 0);
                $modified = 1;
            }
        }
    }
    return unless($modified);
    info("Re-generating consolidated hash");
    my $pkgs = get_pkglist_hashref();
    # retain lock, we need consistency
    $data = read_cache("archive", "", 1);
    my $g = {};
    foreach my $suite (keys(%$data)) {
        next unless($ttl{$suite});
        foreach my $pkg (keys(%{$data->{$suite}})) {
            next if($pkg =~ m#^/#);
            next if(%$pkgs and not $pkgs->{$pkg});
            $g->{$pkg}{$suite} = $data->{$suite}{$pkg};
        }
    }
    # Hash for comparing equivalent versions in different suites
    my %src_compare = (
        oldstable   => 1, # not 0, so no need to test defined()
        sarge       => 1,
        stable      => 2,
        etch        => 2,
        testing     => 3,
        lenny       => 3,
        experimental => 4,
        incoming    => 5,
        new         => 6,
        unstable    => 7,
        sid         => 8,
        other       => 9
    );
    foreach my $pkg (keys(%$g)) {
        my @recent = sort( {
                deb_compare_nofail($g->{$pkg}{$a}, $g->{$pkg}{$b}) or
                ($src_compare{$a} || $src_compare{other}) <=>
                ($src_compare{$b} || $src_compare{other})
            } keys(%{$g->{$pkg}}));
        $g->{$pkg}{most_recent} = $g->{$pkg}{$recent[-1]};
        $g->{$pkg}{most_recent_src} = $recent[-1];
    }
    $data = update_cache("consolidated", $g, "archive", 1, 0);
    unlock_cache("archive");
    return;
}
# Returns the consolidated hash of versions. Doesn't download anything.
sub deb_get_consolidated {
    my $path = shift || "";
    return read_cache("consolidated", "archive/$path", 0);
}
# Returns the hash of versions. Doesn't download anything.
sub deb_get {
    return read_cache("archive", shift, 0);
}
sub get_sources {
    my($suite) = shift;
    my @sections = split(/\s*,\s*/, $CFG{archive}{sections});
    my %vers;
    foreach my $section(@sections) {
        my $url = $CFG{archive}{mirror} . "/dists/$suite/$section/source/Sources.gz";
        info("Downloading $url");
        open(TMP, "+>", undef) or die $!;
        my $res = $ua->get($url, ":content_cb" => sub {
                print TMP $_[0] or die $!;
            });
        unless($res->is_success()) {
            warn "Can't download $url: " . $res->message();
            return 0;
        }
        seek(TMP, 0, SEEK_SET) or die "Can't seek: $!\n";
        my $gz = Compress::Zlib::gzopen(\*TMP, "rb")
            or die "Can't open compressed file: $!\n";

        my $data;
        open($data, "+>", undef) or die $!;
        my $buffer = " " x 4096;
        my $bytes;
        while(($bytes = $gz->gzread($buffer)) > 0) {
            print $data $buffer;
        }
        die $gz->gzerror if($bytes < 0);
        close TMP;
        #my $z = new IO::Uncompress::Gunzip(\$data);

        seek($data, 0, SEEK_SET) or die "Can't seek: $!\n";
        # Blank line as "line" separator, so a "line" is a full record
        local $/ = "";
        while(<$data>) {
            s/\n\s+//gm;
            /^package:\s*(\S+)\s*$/mi or next;
            my $pkg = $1;
            /^version:\s*(\S+)\s*$/mi or next;
            $vers{$pkg} = $1;
        }
        close $data;
    }
    return \%vers;
}
sub get_incoming {
    my $url = $CFG{archive}{incoming_url};
    info("Downloading $url");
    my $res = $ua->get($url);
    unless($res->is_success()) {
        warn "Can't download $url: " . $res->message();
        return 0;
    }
    my $data = $res->decoded_content();
    my %vers;
    while($data =~ /<a href="([^_]+)_(.+)\.dsc">/g) {
        debug("existing $1: $vers{$1} / $2") if(defined($vers{$1}));
        if(!defined $vers{$1} or deb_compare($2, $vers{$1}) > 0) {
            debug("replaced $1: $vers{$1} -> $2") if(defined($vers{$1}));
            $vers{$1} = $2;
        }
    }
    return \%vers;
}
sub get_new {
    my $url = $CFG{archive}{new_url};
    info("Downloading $url");
    my $res = $ua->get($url);
    unless($res->is_success()) {
        warn "Can't download $url: " . $res->message();
        return 0;
    }
    my $data = $res->decoded_content();
    my $te = new HTML::TableExtract( headers => [ qw(
        Package Version Arch Distribution Age Maintainer Closes
        ) ]);
    $te->parse($data);
    my %vers;
    foreach my $table ($te->tables) {
        foreach my $row ($table->rows) {
            next unless $row->[2] =~ /source/;
            my $pkg = $row->[0];
            foreach(split(/\s+/, $row->[1])) {
                next unless($_);
                debug("existing $pkg: $vers{$pkg} / $_") if(
                    defined($vers{$pkg}));
                if(!defined $vers{$pkg} or deb_compare($_, $vers{$pkg}) > 0) {
                    debug("replaced $pkg: $vers{$pkg} -> $_") if(
                        defined($vers{$pkg}));
                    $vers{$pkg} = $_;
                }
            }
        }
    }
    return \%vers;
}
1;
