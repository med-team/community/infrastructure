# vim:ts=4:sw=4:et:ai:sts=4
# $Id: BTS.pm 12750 2008-01-14 20:54:11Z tincho-guest $
#
# Module for retrieving bugs from the BTS, using the SOAP interface
#
# Copyright gregor herrmann <gregor+debian@comodo.priv.at>, 2007
# Copyright Damyan Ivanov <dmn@debian.org>, 2007
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Released under the terms of the GNU GPL 2

package DebianQA::BTS;
use strict;
use warnings;

our @ISA = "Exporter";
our @EXPORT = qw(bts_download bts_get bts_get_consolidated);

use DebianQA::Common;
use DebianQA::Config '%CFG';
use DebianQA::Cache;
use DebianQA::Svn;
use SOAP::Lite;

#my $maint = 'pkg-perl-maintainers@lists.alioth.debian.org';

sub bts_download {
    my($force, @pkglist) = @_;
    $force ||= 0;
    debug("bts_download($force, (@pkglist))");

    my @list;
    my $cdata = {};
    my $replace = 0;

    my $soap = SOAP::Lite->uri($CFG{bts}{soap_uri})->proxy(
        $CFG{bts}{soap_proxy});
    unless($force) {
        $cdata = read_cache("bts", "", 0);
    }
    my $pkginfo = get_pkglist_hashref();
    if(@pkglist) {
        # A list of packages to update has been received
        unless($force) {
            @pkglist = grep( {
                    $CFG{bts}{ttl} * 60 < time - find_stamp($cdata, $_)
                } @pkglist);
            return $cdata unless(@pkglist); # Cache is up-to-date
            info("BTS info for @pkglist is stale") if(@pkglist);
        }
        info("Downloading list of bugs of (", join(", ", @pkglist),
            ")");
        @list = @{$soap->get_bugs( src => [ @pkglist ] )->result()};
    } elsif($force or $CFG{bts}{ttl} * 60 < time - find_stamp($cdata, "")) {
        # No list of packages; forced operation or stale cache
        info("BTS info is stale") unless($force);
        $replace = 1;
        @pkglist = keys %$pkginfo;
        # TODO: could verificate that pkglist and maint = $maint are the same
        # packages
        if(@pkglist) {
            info("Downloading list of bugs of packages in the repo");
            @list = @{$soap->get_bugs( src => [ @pkglist ] )->result()};
        } else {
            # Doesn't make sense to search bugs if we don't have the list
            # of packages.
            return {};
#            info("Downloading list of bugs assigned to $maint");
#            @list = @{$soap->get_bugs( maint => $maint )->result()};
        }
    } else {
        # Cache is up to date
        return $cdata;
    }
    my $bugs_st = {};
    if(@list) {
        info("Downloading status for ", scalar @list, " bugs");
        $bugs_st = $soap->get_status(@list)->result();
    }

    my %binmap;
    foreach my $src (keys %$pkginfo) {
        $binmap{$_} = $src foreach(@{$pkginfo->{$src}{binaries} || []});
    }
    my %bugs = ();
    foreach my $bug (keys %$bugs_st) {
        # Until #458822 is solved, we need to use our own bin -> src mapping
        my $binname = $bugs_st->{$bug}->{package};
        # There could be more than one package!
        $binname =~ s/\s+//g;
        my @binnames = split(/,/, $binname);
        my $found = 0;
        foreach(@binnames) {
            my $srcname = exists $pkginfo->{$_} ? $_ : $binmap{$_} or next;
            $bugs{$srcname}{$bug} = $bugs_st->{$bug};
            $found++;
        }
        unless($found) {
            warn("Can't find source package for $binname in bug #$bug");
            next;
        }
    }
    # retain lock, we need consistency
    $cdata = update_cache("bts", \%bugs, "", $replace, 1);

    info("Re-generating consolidated hash");
    @pkglist = keys %$pkginfo;

    # TODO: Interesting fields:
    # keywords/tags, severity, subject, forwarded, date
    my %cbugs;
    foreach my $pkgname (@pkglist) {
        $bugs{$pkgname} ||= {};

        # bugs to ignore if keyword present
        my %ign_keywords = map({ $_ => 1 }
            split(/\s*,\s*/, $CFG{bts}{ignore_keywords}));
        # bugs to ignore if of specified severities
        my %ign_severities = map({ $_ => 1 }
            split(/\s*,\s*/, $CFG{bts}{ignore_severities}));

        $cbugs{$pkgname} = {};
        foreach my $bug (keys %{ $bugs{$pkgname} }) {
            next unless(ref $bugs{$pkgname}{$bug});
            # Remove done bugs
            next if($bugs{$pkgname}{$bug}{done});
            # Remove if severity match
            next if($ign_severities{$bugs{$pkgname}{$bug}{severity}});
            # Remove if keyword match
            my @keywords = split(/\s+/, $bugs{$pkgname}{$bug}{keywords});
            next if(grep({ $ign_keywords{$_} } @keywords));
            $cbugs{$pkgname}{$bug} = {
                keywords => $bugs{$pkgname}{$bug}{keywords},
                # need to use a new key for compatibility
                keywordsA => \@keywords,
                severity => $bugs{$pkgname}{$bug}{severity},
                subject  => $bugs{$pkgname}{$bug}{subject},
                forwarded=> $bugs{$pkgname}{$bug}{forwarded},
            };
        }
    }
    update_cache("consolidated", \%cbugs, "bts", 1, 0);
    unlock_cache("bts");
    return $cdata;
}
# Returns the hash of bugs. Doesn't download anything.
sub bts_get {
    return read_cache("bts", shift, 0);
}
# Returns the consolidated hash of bugs. Doesn't download anything.
sub bts_get_consolidated {
    my $path = shift || "";
    return read_cache("consolidated", "bts/$path", 0);
}
1;
