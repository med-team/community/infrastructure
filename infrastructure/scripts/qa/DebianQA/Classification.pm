# vim:ts=4:sw=4:et:ai:sts=4
# $Id: Classification.pm 12348 2008-01-09 22:42:14Z tincho-guest $
#
# Module for classifying packages into problem clases. The idea is to make the
# reporting scripts absolutely minimal, and to have a common code in different
# report implementations.
#
# Copyright gregor herrmann <gregor+debian@comodo.priv.at>, 2007
# Copyright Damyan Ivanov <dmn@debian.org>, 2007
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Released under the terms of the GNU GPL 2

package DebianQA::Classification;
use strict;
use warnings;

our @ISA = "Exporter";
our @EXPORT = qw(classify);

use DebianQA::Cache;
#use DebianQA::Common;
#use DebianQA::Config '%CFG';
use DebianQA::DebVersions;

# Takes a list of packages to process.
# Returns an unique hash ready to use in reporting, keyed by package name.
# package_name => {
#   status => {                 # Hash to ease lookup, empty if OK (@notes)
#       needs_upload => 1,
#       needs_upgrade => 1,
#       invalid_svn_version => 1,
#       ...
#   },
#   notes => [ ... ],
#   hilight => {                # Problems indexed by highlighted item
#       archive => { needs_upload => 1, ... },
#       bts => { has_bugs => 1 }, ...
#   },
#   svn_path => "...",
#   upstream_url => "...",      # Already extracted data for ease of use
#
#   bts => {},
#   archive => {},
#   svn => {},
#   watch => {}                 # Copies from the caches
# }

my %error_hilight = (
    archive_waiting => "archive",
    needs_upload => "archive",
    never_uploaded => "archive",
    has_bugs => "bts",
    not_finished => "svn",
    repo_ancient => "svn",
    needs_upgrade => "upstream",
    upstream_ancient => "upstream",
    watch_error => "upstream",
#    native => "",
);

sub classify(@) {
    my @pkglist = @_;
    my $data = read_cache(consolidated => "");
    my %res = ();

    foreach my $pkg (@pkglist) {
        next if($pkg =~ /^\//);
        my(%status, @notes);
        # SVN versus archive
        my $archive_ver = $data->{archive}{$pkg}{most_recent};
        my $svn_ver = $data->{svn}{$pkg}{version};
        my $svn_unrel_ver = $data->{svn}{$pkg}{un_version};
        if(not $svn_ver or not $archive_ver) {
            if(not $svn_ver) {
                $status{not_finished} = 1;
            }
            if(not $archive_ver) {
                $status{never_uploaded} = 1;
            }
        } elsif(deb_compare($archive_ver, $svn_ver) > 0) {
            $status{repo_ancient} = 1;
            push @notes, "$archive_ver > $svn_ver";
        } elsif(deb_compare($archive_ver, $svn_ver) != 0
                and not $svn_unrel_ver) {
            $status{needs_upload} = 1;
        }
        # SVN versus upstream
        my $repo_mangled_ver = $data->{svn}{$pkg}{mangled_ver};
        my $repo_unrel_mangled_ver = $data->{svn}{$pkg}{mangled_un_ver};
        my $upstream_mangled_ver = $data->{watch}{$pkg}{upstream_mangled};
        # watch_error from svn is not needed, as Watch.pm copies it
        my $watch_error = $data->{watch}{$pkg}{error};
        if($watch_error and $watch_error eq "Native") {
            #$status{native} = 1;
        } elsif($watch_error) {
            $status{watch_error} = 1;
            push @notes, "Watch problem: $watch_error";
        } elsif((not $repo_mangled_ver and not $repo_unrel_mangled_ver)
                or not $upstream_mangled_ver) {
            $status{watch_error} = 1; # Should not happen
            push @notes, "Unexpected watchfile problem";
        } elsif($repo_mangled_ver) { # Will not check if UNRELEASED (?)
            if(deb_compare($repo_mangled_ver, $upstream_mangled_ver) > 0) {
                $status{upstream_ancient} = 1;
                push @notes, "$repo_mangled_ver > $upstream_mangled_ver";
            }
            if(deb_compare($repo_mangled_ver, $upstream_mangled_ver) < 0) {
                $status{needs_upgrade} = 1;
            }
        }
        # Archive
        my $archive_latest = $data->{archive}{$pkg}{most_recent_src} || "";
        if($archive_latest =~ /new|incoming/) {
            $status{archive_waiting} = 1;
        }
        if($data->{bts}{$pkg} and %{$data->{bts}{$pkg}}) {
            $status{has_bugs} = 1;
        }
        my %hilight;
        foreach(keys %status) {
            die "Internal error: $_ is not a valid status" unless(
                $error_hilight{$_});
            $hilight{$error_hilight{$_}}{$_} = 1;
        }
        $res{$pkg} = {
            watch   => $data->{watch}{$pkg},
            archive => $data->{archive}{$pkg},
            svn     => $data->{svn}{$pkg},
            bts     => $data->{bts}{$pkg},
            #
            svn_path => $data->{svn}{$pkg}{dir},
            upstream_url => $data->{watch}{$pkg}{upstream_url},
            #
            status  => \%status,
            notes   => \@notes,
            hilight => \%hilight
        };
    }
    return \%res;
}

1;
