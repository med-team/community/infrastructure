# vim:ts=4:sw=4:et:ai:sts=4
# $Id: Common.pm 12770 2008-01-15 13:35:02Z tincho-guest $
#
# Common helper routines
#
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Released under the terms of the GNU GPL 2

package DebianQA::Common;
use strict;
use warnings;

our @ISA = "Exporter";
our @EXPORT = qw(debug info warn error $VERSION);
#our $VERSION = join(".", q$Revision: 12770 $ =~ /(\d+)/g);

# Cannot use this on alioth
#use version;
#our $VERSION = qv("1.000");

# Bump this version in case of data file change
our $VERSION = 1.002;

use DebianQA::Config '%CFG';
use POSIX;

my $basename;

sub print_msg {
    my($level, @msg) = @_;
    return if($level > $CFG{common}{verbose});
    unless($basename) {
        $basename = $0;
        $basename =~ s{.*/+}{};
    }
    @msg = split(/\n+/, join("", @msg));
    foreach(@msg) {
        if($CFG{common}{formatted_log}) {
            printf(STDERR "%s %s[%d]: %s\n",
                strftime("%b %e %H:%M:%S", localtime), $basename, $$, $_);
        } else {
            printf(STDERR $_);
        }
    }
}
sub error {
    print_msg(0, @_);
}
sub warn {
    print_msg(1, @_);
}
sub info {
    print_msg(2, @_);
}
sub debug {
    print_msg(3, @_);
}
1;
