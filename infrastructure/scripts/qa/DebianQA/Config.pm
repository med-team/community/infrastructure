# vim:ts=4:sw=4:et:ai:sts=4
# $Id: Config.pm 12750 2008-01-14 20:54:11Z tincho-guest $
#
# Module that holds configuration variables. Also has subroutines for parsing
# command line options and the configuration file.
#
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Released under the terms of the GNU GPL 2
package DebianQA::Config;

use strict;
use warnings;

use FindBin;
use Getopt::Long;

our @EXPORT = qw(%CFG read_config getopt_common);
our @ISA = "Exporter";

# Default values
my %defaults = (
    qareport_cgi => {
        templates_path => "templates",
        default_template => "by_category",
        group_name => "Unnamed Packaging Group",
        group_url => "http://www.debian.org/",
        wsvn_url => undef,
    },
    svn => {
        repository => undef,
        packages_path => "trunk",
        post_path => ""
    },
    archive => {
        mirror => "ftp://ftp.debian.org/debian",
        suites => "unstable, testing, stable, oldstable, experimental",
        sections => "main, contrib, non-free",
        suites_ttl => "360, 360, 10080, 10080, 360",
        new_url => 'http://ftp-master.debian.org/new.html',
        new_ttl => 60,
        incoming_url => 'http://incoming.debian.org',
        incoming_ttl => 60,
    },
    watch => {
        ttl => 360,
        use_cpan => 1,
        cpan_mirror => "ftp://cpan.org/ls-lR.gz",
        cpan_ttl => 360 # 6 hours
    },
    bts => {
        ttl => 360, # 6 hours
        soap_proxy => 'http://bugs.debian.org/cgi-bin/soap.cgi',
        soap_uri => 'Debbugs/SOAP',
        ignore_keywords => "",
        ignore_severities => ""
    },
    common => {
        cache_dir => "$ENV{HOME}/.debianqa/yourgroup",
        # verbosity level: error => 0, warn => 1, info => 2, debug => 3
        # Should be 1 by default, 0 for quiet mode
        verbose => 1,
        # Prepend syslog-style format?
        formatted_log => 1
    }
);
our %CFG = %defaults; # Global configuration
my %valid_cfg;
foreach my $section (keys %defaults) {
    $valid_cfg{$section} = { map({ $_ => 1 } keys(%{$defaults{$section}})) };
}

sub read_config(;$) {
    my $file = shift;
    unless($file) {
        if($ENV{DEBIAN_QA_CONF}) {
            $file = $ENV{DEBIAN_QA_CONF};
        } elsif(-e "$ENV{HOME}/.debianqa/debianqa.conf") {
            $file = "$ENV{HOME}/.debianqa/debianqa.conf";
        } elsif(-e "/etc/debianqa.conf") {
            $file = "/etc/debianqa.conf";
        } elsif(-e "debianqa.conf") {
            $file = "debianqa.conf";
        } elsif(-e "$FindBin::Bin/debianqa.conf") {
            $file = "$FindBin::Bin/debianqa.conf";
        } else {
            die "Can't find any configuration file!\n";
        }
    }
    die "Can't read configuration file: $file\n" unless(-r $file);

    my $section = "common";
    open(CFG, "<", $file) or die "Can't open $file: $!\n";
    while(<CFG>) {
        chomp;
        s/(?<!\S)[;#].*//;
        s/\s+$//;
        next unless($_);
        if(/^\s*\[\s*(\w+)\s*\]\s*$/) {
            $section = lc($1);
            die "Invalid section in configuration file: $section\n" unless(
                exists($valid_cfg{$section}));
            next;
        }
        unless(/^\s*([^=]+?)\s*=\s*(.*)/) {
            die "Unrecognised line in configuration file: $_\n";
        }
        my($key, $val) = ($1, $2);
        unless(exists($valid_cfg{$section}{$key})) {
            die("Unrecognised configuration parameter $key in section " .
                "$section\n");
        }
        if($val =~ s/^~\///) { # UGLY!
            $val = $ENV{HOME} . "/$val";
        }
        $CFG{$section}{$key} = $val;
    }
    close CFG;
}
# Parses command line options, loads configuration file if specified, removes
# arguments from @ARGV and returns a hash with the parsed options.
# If $passthru, ignores unknown parameters and keeps them in @ARGV.
# If $readconf, will call read_config even if the user didn't say --conf
sub getopt_common(;$$) {
    my($passthru, $readconf) = @_;
    my($conffile, $force, $v, $q) = (undef, 0, 0, 0);
    my $p = new Getopt::Long::Parser;
    $p->configure(qw(no_ignore_case bundling),
        $passthru ? ("pass_through") : ());
    $p->getoptions(
        'conf|c=s' => \$conffile, 'force|f!' => \$force,
        'verbose|v:+' => \$v, 'quiet|q:+' => \$q
    ) or die("Error parsing command-line arguments\n");
    read_config($conffile) if($conffile or $readconf);
    $CFG{common}{verbose} += $v - $q;
    return {
        force => $force     # only one argument for now
    };
}
1;
