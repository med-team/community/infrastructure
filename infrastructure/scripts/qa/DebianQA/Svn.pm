# vim:ts=4:sw=4:et:ai:sts=4
# $Id: Svn.pm 12767 2008-01-15 13:12:33Z tincho-guest $
#
# Module for retrieving data from the SVN repository. It understands SVN
# revisions and uses them instead of timestamps for checking cache validity. It
# parses changelog and watch files.
#
# Copyright gregor herrmann <gregor+debian@comodo.priv.at>, 2007
# Copyright Damyan Ivanov <dmn@debian.org>, 2007
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Released under the terms of the GNU GPL 2

package DebianQA::Svn;
use strict;
use warnings;

our @ISA = "Exporter";
our @EXPORT = (qw(
    svn_download svn_get svn_get_consolidated
    svndir2pkgname pkgname2svndir get_pkglist get_pkglist_hashref
    ));

use IO::Scalar;
use Digest::MD5 "md5_hex";
use Parse::DebianChangelog;
use DebianQA::Cache;
use DebianQA::Common;
use DebianQA::Config '%CFG';
use DebianQA::DebVersions;
use Parse::DebControl;
use SVN::Client;

# Returns the list of changed directories
sub svn_download {
    my($force, $revision, @dirlist) = @_;
    $force ||= 0;
    $revision ||= 0;
    debug("svn_download($force, $revision, (@dirlist))");

    die "Missing SVN repository" unless($CFG{svn}{repository});
    my $svnpath = $CFG{svn}{repository};

    # Sanitise, as SVN::Client is too stupid
    $svnpath =~ s{/+$}{};
    $svnpath .= "/";
    $svnpath .= $CFG{svn}{packages_path} if($CFG{svn}{packages_path});
    $svnpath =~ s{/+$}{};
    my $svnpostpath = $CFG{svn}{post_path} || "";
    # Always has a slash if not empty
    $svnpostpath =~ s{^/*(.*?)/*$}{/$1} if($svnpostpath);

    my $complete = ! @dirlist;

    our $svn = SVN::Client->new();
    unless($revision) {
        info("Retrieving last revision number from SVN");
        $svn->info($svnpath, undef, "HEAD", sub {
                $revision = $_[1]->rev();
            }, 0);
    }

    if($complete) {
        info("Retrieving list of directories in SVN");
        my %dirlist = %{$svn->ls($svnpath, 'HEAD', 0)};
        @dirlist = grep({ $dirlist{$_}->kind() == $SVN::Node::dir }
            keys(%dirlist));
        info(scalar @dirlist, " directories to process");
    }
    my(%changed, %svn);

    if($force) {
        %changed = map({ $_ => 1 } @dirlist);
    } else {
        my $cdata = read_cache("svn", "", 0);
        if(find_stamp($cdata, "") == $revision
                and keys(%$cdata) > @dirlist + 1) {
            return (); # Cache is up-to-date
        }

        # Stamps from cache
        my %cache_vers = map({ $_ => find_stamp($cdata, $_) }
            grep({ $cdata->{$_} } @dirlist));
        # Never updated
        %changed = map({ $_ => 1 } grep( { not $cache_vers{$_} } @dirlist));

        # Now search in the SVN log to see if there's any interesting change
        # Remove from list already updated parts of the cache
        # Also remove invalid dirs
        my %invalid;
        foreach my $dir (grep({ $cache_vers{$_}
                    and $cache_vers{$_} < $revision } @dirlist)) {
            $dir =~ s{^/*(.*?)/*$}{$1};
            my $pkghome = "$svnpath/$dir$svnpostpath";
            safe_svn_op($svn, "log", [ $pkghome ], $cache_vers{$dir},
                "HEAD", 1, 1, sub {
                    foreach (keys %{$_[0]}) {
                        $changed{$dir} = 1 if(m{/debian/(changelog|control|watch)$});
                    }
                }) or $invalid{$dir} = 1;
        }
        foreach(keys %invalid) {
            info("Removing invalid $_ directory");
            $svn{$_} = {};
        }
        # Copy the not-changed dirs that we want to have the stamp bumped
        foreach(grep({ ! $changed{$_} } @dirlist)) {
            $svn{$_} = $cdata->{$_} if($cdata->{$_});
        }
    }
    my @changed = keys %changed;
    foreach my $dir (@changed) {
        $dir =~ s{^/*(.*?)/*$}{$1};
        my $debdir = "$svnpath/$dir$svnpostpath/debian";
        $svn{$dir} = {};

        info("Retrieving control information for $dir");
        my $control = get_svn_file($svn, "$debdir/control");

        unless($control) {
            $svn{$dir}{error} = "MissingControl";
            # Check if it's an invalid dir
            safe_svn_op($svn, "ls", $debdir, 'HEAD', 0) and next;
            info("Removing invalid $dir directory");
            $svn{$dir} = {};
            next;
        }

        info("Retrieving changelog for $dir");
        my $changelog = get_svn_file($svn, "$debdir/changelog");

        unless($changelog) {
            $svn{$dir}{error} = "MissingChangelog";
            next;
        }

        # Parse::DebControl hands back a strange structure... A hash-like
        # thing, where [0] includes the debian/control fields for the
        # source package and [1] for the first binary package (and, were 
        # they to exist, [2] and on for the other binary packages - which 
        # we will wisely ignore)
        my ($ctrl_data, $short, $long);
        $control =~ s/^#.*\n//gm; # stripComments looks like nonsense to me
        $ctrl_data = Parse::DebControl->new->parse_mem($control, {
                discardCase => 1 # unreliable if don't
            });
        ($short, $long) = split_description($ctrl_data->[1]{description});

        $svn{$dir}{pkgname} = $ctrl_data->[0]{source};
        my @section = split(/\s*\/\s*/, $ctrl_data->[0]{section});
        unshift @section, "main" unless(@section > 1);
        $svn{$dir}{section} = $section[0];
        $svn{$dir}{subsection} = $section[1];
        $svn{$dir}{uploaders} = $ctrl_data->[0]{uploaders};
        $svn{$dir}{maintainer} = $ctrl_data->[0]{maintainer};
        $svn{$dir}{std_version} = $ctrl_data->[0]{'standards-version'};
        $svn{$dir}{b_d} = $ctrl_data->[0]{'build-depends'};
        $svn{$dir}{b_d_i} = $ctrl_data->[0]{'build-depends-indep'};
        $svn{$dir}{short_descr} = $short;
        $svn{$dir}{long_descr} = $long;
        my %bins;
        foreach(1..$#$ctrl_data) {
            my $bin = $ctrl_data->[$_];
            my ($shd, $lnd) = split_description($bin->{description});
            $svn{$dir}{bindata}[$_-1] = {
                %$bin,
                short_descr => $shd,
                long_descr => $lnd,
            };
            delete $svn{$dir}{bindata}[$_-1]{description};
            $bins{$bin->{package}} = 1;
            if($bin->{provides}) {
                foreach(split(/\s*,\s*/, $bin->{provides})) {
                    $bins{$_} = 1;
                }
            }
        }
        $svn{$dir}{binaries} = [ sort keys %bins ];
        my $parser = Parse::DebianChangelog->init({
                instring => $changelog });
        my $error = $parser->get_error() or $parser->get_parse_errors();
        if($error) {
            error($error);
            $svn{$dir}{error} = "InvalidChangelog";
            next;
        }

        my($lastchl, $unfinishedchl);
        foreach($parser->data()) {
            if($_->Distribution =~ /^(?:unstable|experimental)$/) {
                $lastchl = $_;
                last;
            }
            if(! $unfinishedchl and $_->Distribution eq "UNRELEASED") {
                $unfinishedchl = $_;
            }
        }
        unless($lastchl or $unfinishedchl) {
            $svn{$dir}{error} = "InvalidChangelog";
            next;
        }
        if($lastchl) {
            $svn{$dir}{version} = $lastchl->Version;
            $svn{$dir}{date}    = $lastchl->Date;
            $svn{$dir}{changer} = $lastchl->Maintainer;
            $svn{$dir}{text}    = join(
                "\n",
                map( $lastchl->$_, qw(Header Changes Trailer) ),
            );
        }
        if($unfinishedchl) {
            $svn{$dir}{un_version} = $unfinishedchl->Version;
            $svn{$dir}{un_date}    = $unfinishedchl->Date;
            $svn{$dir}{un_changer} = $unfinishedchl->Maintainer;
            $svn{$dir}{un_text}    = join(
                "\n",
                map( $unfinishedchl->$_, qw(Header Changes Trailer) ),
            );
        }
        if($svn{$dir}{pkgname} ne $parser->dpkg()->{Source}) {
            $svn{$dir}{error} = "SourceNameMismatch";
            next;
        }

        info("Retrieving watchfile for $dir");
        my $watchdata = get_svn_file($svn, "$debdir/watch");
        unless($watchdata) {
            if($svn{$dir}{version} and $svn{$dir}{version} !~ /-/) {
                $svn{$dir}{watch_error} = "Native";
            } else {
                $svn{$dir}{watch_error} = "Missing";
            }
            next;
        }
        my $watch = parse_watch($svn{$dir}{version}, $watchdata);
        # Returns undef on error
        unless($watch and @$watch) {
            $svn{$dir}{watch_error} = "Invalid";
            next;
        }
        my @versions = sort({ deb_compare_nofail($a, $b) }
            grep(defined, map({ $_->{mangled_ver} } @$watch)));

        $svn{$dir}{mangled_ver} = $versions[-1];
        $svn{$dir}{watch} = $watch;

        # Again for unreleased
        $watch = parse_watch($svn{$dir}{un_version}, $watchdata) if(
            $svn{$dir}{un_version});
        # Returns undef on error
        if($watch and @$watch) {
            @versions = sort({ deb_compare_nofail($a, $b) }
                grep(defined, map({ $_->{mangled_ver} } @$watch)));
            $svn{$dir}{mangled_un_ver} = $versions[-1];
        }
    }
    # Retain lock
    my $cdata = update_cache("svn", \%svn, "", $complete, 1, $revision);

    my @pkglist = grep({ ref $cdata->{$_} and $cdata->{$_}{pkgname} }
        keys(%$cdata));
    my %pkglist;
    foreach(@pkglist) {
        $pkglist{$cdata->{$_}{pkgname}} = {
            svndir => $_,
            binaries => $cdata->{$_}{binaries}
        };
    }
    update_cache("consolidated", \%pkglist, "pkglist", 1, 1);
    my %svn2;
    foreach(keys(%$cdata)) {
        next unless ref($cdata->{$_});
        my $pkgname = $cdata->{$_}{pkgname} or next;
        # Shallow copy, it's enough here, but can't be used for anything else
        $svn2{$pkgname} = { %{$cdata->{$_}} };
        $svn2{$pkgname}{dir} = $_;
        delete $svn2{$pkgname}{$_} foreach(
            qw(watch pkgname text un_text long_descr bindata)
        );
    }
    update_cache("consolidated", \%svn2, "svn", 1, 0);
    unlock_cache("svn");
    return @changed;
}
# Returns the hash of svn info. Doesn't download anything.
sub svn_get {
    return read_cache("svn", shift, 0);
}
# Returns the consolidated hash of svn info. Doesn't download anything.
sub svn_get_consolidated {
    my $path = shift || "";
    return read_cache("consolidated", "svn/$path", 0);
}
# Searches the source package name given a svn directory name
# Returns undef if not found
sub svndir2pkgname($) {
    my $dir = shift;
    my $data = read_cache("svn", $dir, 0);
    return $data->{pkgname};
}
# Searches the svn directory name given a source package name
# Returns undef if not found
sub pkgname2svndir($) {
    my $pkg = shift;
    my $data = read_cache("svn", "", 0);
    my @dirs = grep({ ref $data->{$_} and $data->{$_}{pkgname} and
            $data->{$_}{pkgname} eq $pkg } keys %$data);
    return $dirs[0] if(@dirs);
    return undef;
}
# Returns the list of source packages detected in the svn repository
sub get_pkglist {
    my $list = get_pkglist_hashref();
    return keys %$list;
}
sub get_pkglist_hashref {
    my $list = read_cache("consolidated", "pkglist", 0);
    foreach(grep({ /^\// } keys %$list)) {
        delete $list->{$_};
    }
    return $list;
}
# Parses watchfile, returns an arrayref containing one element for each source,
# consisting of the URL spec, an MD5 sum of the line (to detect changes from
# the watch module), the mangled debian version, and a hash of options.
sub parse_watch($$) {
    my($version, $watch) = @_;
    $version ||= '';
    $watch ||= '';
    debug("parse_watch('$version', '...')");
    $watch =~ s/\\\n//gs;

    # Strip epoch and debian release
    $version =~ s/^(?:\d+:)?(.+?)(?:-[^-]+)?$/$1/;

    my @watch_lines = split(/\n/, $watch);
    @watch_lines = grep((!/^#/ and !/^version\s*=/ and !/^\s*$/),
        @watch_lines);

    my @wspecs;
    foreach(@watch_lines) {
        debug("Watch line: $_");

        # opts either contain no spaces, or is enclosed in double-quotes
        my $opts = $1 if(s!^\s*opts="([^"]*)"\s+!! or
            s!^\s*opts=(\S*)\s+!!);
        debug("Watch line options: $opts") if($opts);

        # several options are separated by comma and commas are not allowed
        # within
        my @opts = split(/\s*,\s*/, $opts) if($opts);
        my %opts;
        foreach(@opts) {
            next if /^(?:active|passive|pasv)$/;
            /([^=]+)=(.*)/;
            my($k, $v) = ($1, $2);
            debug("Watch option $k: $v");
            if($k eq 'versionmangle') {
                push @{$opts{uversionmangle}}, $v;
                push @{$opts{dversionmangle}}, $v;
            } else {
                push @{$opts{$k}}, $v;
            }
        }
        my $mangled = $version;
        if($version and $opts{dversionmangle}) {
            foreach(split(/;/, join(";", @{$opts{dversionmangle}}))) {
                debug("Executing \$mangled =~ $_");
                eval "\$mangled =~ $_";
                if($@) {
                    error("Invalid watchfile: $@");
                    return undef;
                }
            }
        }
        debug("Mangled version: $mangled");
        push @wspecs, {
            line => $_,
            mangled_ver => $mangled,
            md5 => md5_hex(($opts || "").$_),
            opts => \%opts
        };
    }
    return \@wspecs;
}
sub get_svn_file($$) {
    my($svn, $target) = @_;
    my $svn_error;
    my $data;
    my $fh = IO::Scalar->new(\$data);
    safe_svn_op($svn, "cat", $fh, $target , 'HEAD');
    return $data;
}
sub safe_svn_op($$@) {
    my($svn, $op, @opts) = @_;
    local $SVN::Error::handler = undef;
    my ($svn_error) = eval "\$svn->$op(\@opts)";
    die $@ if($@);
    if(SVN::Error::is_error($svn_error)) {
        if($svn_error->apr_err() == $SVN::Error::FS_NOT_FOUND) {
            $svn_error->clear();
            return 0;
        } else {
            # dapal FIXME - probably broken :)
            #SVN::Error::croak_on_error($svn_error);
            return 0;
        }
    }
    return 1;
}

sub split_description($) {
    # The 'description' field in debian/control is, IMHO, wrongly handled - Its
    # first line is the short description, and the rest (second to last lines)
    # is the long description. So... Here we just split it, for proper 
    # handling. 
    # 
    # Gets the full description as its only parameter, returns the short and 
    # the long descriptions.
    my ($str, $offset, $short, $long);
    $str = shift;
    $offset = index($str, "\n");
    $short = substr($str, 0, $offset);
    $long = substr($str, $offset+1);
    return ($short, $long);
}

1;
