# vim:ts=4:sw=4:et:ai:sts=4
# $Id: Watch.pm 11498 2007-12-23 10:41:25Z tincho-guest $
#
# Module for scanning watch files and checking upstream versions.
#
# Copyright gregor herrmann <gregor+debian@comodo.priv.at>, 2007
# Copyright Damyan Ivanov <dmn@debian.org>, 2007
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Released under the terms of the GNU GPL 2

package DebianQA::Watch;
use strict;
use warnings;

our @ISA = "Exporter";
our @EXPORT = qw(watch_download watch_get watch_get_consolidated);

use Compress::Zlib ();
use CPAN::DistnameInfo;
use DebianQA::Cache;
use DebianQA::Common;
use DebianQA::Config '%CFG';
use DebianQA::Svn;
use DebianQA::DebVersions;;
use Fcntl qw(:seek);
use LWP::UserAgent;

my $cpanregex = qr#^((?:http|ftp)://\S*(?:cpan|backpan)\S*)/(dist|modules/by-module|(?:by-)?authors/id)\b#i;

my $ua = new LWP::UserAgent;
$ua->timeout(10);
$ua->env_proxy;

sub watch_download {
    my($force, @pkglist) = @_;
    $force ||= 0;
    debug("watch_download($force, (@pkglist))");

    if($CFG{watch}{use_cpan}) {
        cpan_dist_download($force);
        cpan_index_download($force);
    }
    my $complete;
    if(not @pkglist) {
        $complete = 1;
        @pkglist = grep(! /^\//, get_pkglist());
    }
    my $cdata = watch_get() unless($force);
    my(%watch, %watch2, @not_updated);
    foreach my $pkg (@pkglist) {
        debug("Retrieving svn info for $pkg");
        my $svndata = svn_get(pkgname2svndir($pkg));
        if($svndata->{watch_error}) {
            $watch2{$pkg} = { error => $svndata->{watch_error} };
            next;
        }
        unless($svndata->{watch} and ref $svndata->{watch}
                and ref $svndata->{watch} eq "ARRAY") {
            $watch2{$pkg} = { error => "Missing" };
            next;
        }
        my @wlines = @{$svndata->{watch}};
        unless(@wlines) {
            $watch2{$pkg} = { error => "Empty" };
            next;
        }
        my @wresult;
        foreach my $wline (@wlines) {
            my $md5 = $wline->{md5};
            next unless($md5);
            if(not $force and $cdata->{$md5} and
                $CFG{watch}{ttl} * 60 > time - find_stamp($cdata, $md5)) {
                $watch{$md5} = $cdata->{$md5};
                push @not_updated, $md5;
            } else {
                my ($watcherr, %uscand) = uscan($wline->{line},
                    %{$wline->{opts}});
                if($watcherr) {
                    warn("Error while processing $pkg watch file: $watcherr");
                } else {
                    info("Found: version $uscand{upstream_version} ",
                        "from $uscand{upstream_url} ",
                        "(mangled: $uscand{upstream_mangled})");
                }
                $watch{$md5} = { watch_error => $watcherr, %uscand };
            }
            my $diff = 0;
            if(not $watch{$md5}{upstream_mangled}) {
                $watch{$md5}{watch_error} ||= "Error";
            } elsif($wline->{mangled_ver}) {
                $diff = deb_compare($wline->{mangled_ver},
                    $watch{$md5}{upstream_mangled});
                $watch{$md5}{watch_error} = "InvalidVersion" unless(
                    defined $diff);
            }
            push @wresult, { diff => $diff, %{$watch{$md5}} };
        }
        my @noerror = grep({ not $_->{watch_error} } @wresult);
        @noerror = sort({
                deb_compare_nofail($a->{upstream_mangled},
                    $b->{upstream_mangled}) } @noerror);
        unless(@noerror) {
            $watch2{$pkg} = { error => $wresult[0]{watch_error} };
            next;
        }
        my @result;
        if(@result = grep({ $_->{diff} < 0 } @noerror)) {
            $watch2{$pkg} = $result[-1];
        } elsif(@result = grep( { not $_->{diff} } @noerror)) {
            $watch2{$pkg} = $result[0];
        } else {
            $watch2{$pkg} = $noerror[0];
        }
        delete($watch2{$pkg}{diff}) unless($watch2{$pkg}{diff});
        delete($watch2{$pkg}{watch_error}) unless($watch2{$pkg}{watch_error});
    }
    delete $watch{$_} foreach(@not_updated);
    update_cache("watch", \%watch, "", $complete && @not_updated == 0, 1);
    update_cache("consolidated", \%watch2, "watch", $complete, 0);
    unlock_cache("watch");
    info("watch: ", scalar @pkglist, " packages scanned");
}
# Returns the hash of bugs. Doesn't download anything.
sub watch_get {
    return read_cache("watch", shift, 0);
}
# Returns the consolidated hash of bugs. Doesn't download anything.
sub watch_get_consolidated {
    my $path = shift || "";
    return read_cache("consolidated", "watch/$path", 0);
}
sub uscan($) {
    my($wline, %opts) = @_;
    info("Processing watch line $wline");

    $wline =~ s{^http://sf\.net/(\S+)}{http://qa.debian.org/watch/sf.php/$1};
    # Fix URIs with no path
    $wline =~ s{^(\w+://[^\s/]+)(\s|$)}{$1/$2};
    unless($wline =~ m{^(?:(?:https?|ftp)://\S+?)/}) {
        warn("Invalid watch line: $wline");
        return("Invalid");
    }
    my @items = split(/\s+/, $wline);

    my($dir, $filter);
    # Either we have single URL/pattern
    # or URL/pattern + extra
    if($items[0] =~ /\(/) {
        # Since '+' is greedy, the second capture has no slashes
        ($dir, $filter) = $items[0] =~ m{^(.+)/(.+)$};
    } elsif(@items >= 2 and $items[1] =~ /\(/) {
        # or, we have a homepage plus pattern
        # (plus optional other non-interesting stuff)
        ($dir, $filter) = @items[0,1];
    }
    unless($dir and $filter) {
        return("Invalid");
    }
    debug("uscan $dir $filter");
    my @vers;
    if($CFG{watch}{use_cpan} and $dir =~ $cpanregex) {
        @vers = cpan_lookup($dir, $filter);
        my $status = shift @vers;
        if($status) {
            warn("CPAN lookup failed for $dir + $filter: $status");
            return $status;
        } elsif(not @vers) {
            warn("CPAN lookup failed for $dir + $filter");
        }
    }
    unless(@vers) {
        @vers = recurse_dirs($filter, $dir, "");
        my $status = shift @vers;
        return $status || "NotFound" unless(@vers);
    }

    my @mangled;
    foreach my $uver (@vers) {
        push @mangled, $uver->{upstream_version};
        next unless($opts{uversionmangle});
        debug("Mangle option: ", join(", ", @{$opts{uversionmangle}}));
        foreach(split(/;/, join(";", @{$opts{uversionmangle}}))) {
            debug("Executing '\$mangled[-1] =~ $_'");
            eval "\$mangled[-1] =~ $_";
            if($@) {
                error("Invalid watchfile: $@");
                return("Invalid");
            }
        }
        debug("Mangled version: $mangled[-1]");
    }
    my @order = sort({ deb_compare_nofail($mangled[$a], $mangled[$b]) }
        (0..$#vers));
    return(undef,
        %{$vers[$order[-1]]},
        upstream_mangled => $mangled[$order[-1]]);
}
sub recurse_dirs($$$);
sub recurse_dirs($$$) {
    my($filter, $base, $remaining) = @_;
    debug("recurse_dirs($filter, $base, $remaining)");

    if($base =~ s{/([^/]*?\(.*)}{}) {
        $remaining = "$1/$remaining";
    }
    my @rparts = split(/\/+/, $remaining) if($remaining);
    while(@rparts and $rparts[0] !~ /\(/) {
        $base .= "/" . shift @rparts;
    }
    if(@rparts) {
        my ($status, @data) = recurse_dirs($rparts[0]."/?", $base, "");
        return $status unless(@data);
        @data = sort({ deb_compare_nofail($a->{upstream_version},
                    $b->{upstream_version}) } @data);
        $base = $data[-1]{upstream_url};
    }
    unless($base =~ m{(^\w+://[^/]+)(/.*?)/*$}) {
        error("Invalid base: $base");
        return("Invalid");
    }
    my $site = $1;
    my $path = $2;
    my $pattern;
    if($filter =~ m{^/}) {
        $pattern = qr{(?:^\Q$site\E)?$filter};
    } elsif($filter !~ m{^\w+://}) {
        $pattern = qr{(?:(?:^\Q$site\E)?\Q$path\E/)?$filter};
    } else {
        $pattern = $filter;
    }

    debug("Downloading $base");
    my $res = $ua->get($base);
    unless($res->is_success) {
        error("Unable to get $base: " . $res->message());
        return ("NotFound") if($res->code == 404);
        return ("DownloadError");
    }
    my $page = $res->decoded_content();
    $page =~ s/<!--.*?-->//gs;
    $page =~ s/\n+/ /gs;

    my @candidates;
    if($base =~ /^ftp/) {
        @candidates = split(/\s+/, $page);
    } else {
        @candidates = grep(defined, ($page =~
                m{<a\s[^>]*href\s*=\s*(?:"([^"]+)"|'([^']+)'|([^"]\S+))}gi));
    }
    my @vers;
    foreach my $url (grep(m{^$pattern$}, @candidates)) {
        $url =~ m{^$pattern$};
        my @ver = map({substr($url, $-[$_], $+[$_] - $-[$_])} (1..$#+));
        if($url =~ m{^/}) {
            $url = $site . $url;
        } elsif($url !~ m{^\w+://}) {
            $url = $site . $path . "/" . $url;
        }
        push @vers, {
            upstream_version => join(".", @ver),
            upstream_url => $url };
    }
    debug("Versions found: ", join(", ", map({ $_->{upstream_version} }
                @vers)));
    return(undef, @vers);
}

sub cpan_lookup($$) {
    my($dir, $filter) = @_;

    $dir =~ $cpanregex or return ();
    my $base = $1;
    my $type = $2;
    $dir =~ s{/+$}{};
    my $origdir = $dir;

    $type =~ s/.*(dist|modules|authors).*/$1/ or return ();
    my $cpan;
    if($type eq "dist") {
        $filter =~ s/.*\///;
        $cpan = cpan_dist_download();
    } else {
        $cpan = cpan_index_download()->{$type};
    }
    $dir =~ s/$cpanregex//i;
    $dir =~ s{^/+}{};
    debug("Looking for $dir + $filter into CPAN $type cache");
    #return ("NotFound") unless(exists($cpan->{$dir}));
    # Allow this to gracefully degrade to a normal uscan check
    # dapal FIXME
    #return () unless(exists($cpan->{$dir}));

    my @res;
    foreach(keys %{$cpan->{$dir}}) {
        next unless ($_ =~ $filter);
        my $filt_ver = $1;
        if($type eq "dist") {
            my $cpan_ver = $cpan->{$dir}{$_}{version};
            if($filt_ver ne $cpan_ver) {
                # Try to remove initial "v"s, if any
                $cpan_ver =~ s/^v//;
            }
            if($filt_ver ne $cpan_ver) {
                warn("Version mismatch: uscan says $filt_ver, ",
                    "cpan says $cpan_ver");
                return ("VersionMismatch");
            }
        }
        push @res, {
            upstream_version => $filt_ver,
            upstream_url => (
                $type eq "dist" ?
                "$base/CPAN/authors/id/" . $cpan->{$dir}{$_}{path} :
                "$origdir/$_"
            )
        };
    }
    # Allow this to gracefully degrade to a normal uscan check
    #return ("NotFound") unless(@res);
    return (undef, @res);
}
sub cpan_dist_download(;$) {
    my $force = shift;
    unless($force) {
        my $cpan = read_cache("cpan_dists", "", 0);
        if($CFG{watch}{cpan_ttl} * 60 > time - find_stamp($cpan, "")) {
            return $cpan;
        }
    }

    # dapal FIXME - can't find that file gzipped anymore, disabled uncompression
    #my $url = $CFG{watch}{cpan_mirror} . "/modules/02packages.details.txt.gz";
    my $url = "http://www.cpan.org/modules/02packages.details.txt";
    my $data;
    info("Rebuilding CPAN dists cache from $url");
    open($data, "+>", undef) or die $!;
    my $res = $ua->get($url, ":content_cb" => sub {
            print $data $_[0] or die $!;
        });
    unless($res->is_success()) {
        warn "Can't download $url: " . $res->message();
        return 0;
    }
    seek($data, 0, SEEK_SET) or die "Can't seek: $!\n";
    #my $gz = Compress::Zlib::gzopen(\*TMP, "rb")
    #    or die "Can't open compressed file: $!\n";

    #my $data;
    #open($data, "+>", undef) or die $!;
    #my $buffer = " " x 4096;
    #my $bytes;
    #while(($bytes = $gz->gzread($buffer)) > 0) {
    #    print $data $buffer;
    #}
    #die $gz->gzerror if($bytes < 0);
    #close TMP;
    ##my $z = new IO::Uncompress::Gunzip(\$data);

    #seek($data, 0, SEEK_SET) or die "Can't seek: $!\n";

    # Skip header
    while(<$data>) {
        chomp;
        last if(/^$/);
    }
    my $cpan = {};
    while(<$data>) {
        chomp;
        my $tarball = (split)[2];
        my $distinfo = new CPAN::DistnameInfo($tarball);
#       next if($distinfo->maturity() eq "developer");
        my $distname = $distinfo->dist();
        unless($distname) {
            info("Invalid CPAN distribution: $tarball");
            next;
        }
        my $version = $distinfo->version();
        my $filename = $distinfo->filename();

        $cpan->{$distname}{$filename} = {
            path => $tarball,
            version => $version
        };
    }
    close $data;
    update_cache("cpan_dists", $cpan, "", 1);
    return $cpan;
}
sub cpan_index_download(;$) {
    my $force = shift;
    unless($force) {
        my $cpan = read_cache("cpan_index", "", 0);
        if($CFG{watch}{cpan_ttl} * 60 > time - find_stamp($cpan, "")) {
            return $cpan;
        }
    }

    my $url = $CFG{watch}{cpan_mirror} . "/indices/ls-lR.gz";
    info("Rebuilding CPAN indices cache from $url");
    open(TMP, "+>", undef) or die $!;
    my $res = $ua->get($url, ":content_cb" => sub {
            print TMP $_[0] or die $!;
        });
    unless($res->is_success()) {
        warn "Can't download $url: " . $res->message();
        return 0;
    }
    seek(TMP, 0, SEEK_SET) or die "Can't seek: $!\n";
    my $gz = Compress::Zlib::gzopen(\*TMP, "rb")
        or die "Can't open compressed file: $!\n";

    my $data;
    open($data, "+>", undef) or die $!;
    my $buffer = " " x 4096;
    my $bytes;
    while(($bytes = $gz->gzread($buffer)) > 0) {
        print $data $buffer;
    }
    die $gz->gzerror if($bytes < 0);
    close TMP;
    #my $z = new IO::Uncompress::Gunzip(\$data);

    seek($data, 0, SEEK_SET) or die "Can't seek: $!\n";

    my $cpan = {};
    my($dir, $type);
    while(<$data>) {
        chomp;
        if(/^(.+):$/) {
            my $subdir = $1;
            $type = undef;
            $subdir =~ m{/.*(authors/id|modules/by-module)/+(.*?)/*$} or next;
            $dir = $2;
            $1 =~ /(authors|modules)/ and $type = $1;
            next;
        }
        next unless($type and /^[-l]r.....r/);
        s/ -> .*//;
        my $file = (split)[8];
        $file =~ m{\.(?:bz2|gz|zip|pl|pm|tar|tgz)$}i or next;
        $cpan->{$type}{$dir}{$file} = 1;
    }
    close $data;
    update_cache("cpan_index", $cpan, "", 1);
    return $cpan;
}
1;
