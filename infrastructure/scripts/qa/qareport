#!/usr/bin/perl
# vim:ts=4:sw=4:et:ai:sts=4
# $Id: qareport 11877 2007-12-31 06:48:26Z tincho-guest $
#
# Draft of a report
#
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Released under the terms of the GNU GPL 2
use strict;
use warnings;

#use DebianQA::Cache;
use DebianQA::Classification;
#use DebianQA::Common;
use DebianQA::Config;
#use DebianQA::DebVersions;
use DebianQA::Svn;
use Getopt::Long;

my $p = new Getopt::Long::Parser;
$p->configure(qw(no_ignore_case bundling pass_through));

my $list_is_dirs = 0;
my $show_all = 0;
$p->getoptions('help|h|?' => \&help, 'directories!' => \$list_is_dirs,
    'showall|a!' => \$show_all
    ) or die "Error parsing command-line arguments!\n";

my $opts = getopt_common(0, 1); # No passthru, load config

my @dirs = @ARGV;

if($list_is_dirs) {
    foreach my $dir (@dirs) {
        $dir = svndir2pkgname($dir) || $dir; # Fallback
    }
}

my @pkglist = @dirs;
@pkglist = get_pkglist() unless(@pkglist);
my $csfy = classify(@pkglist);
unless($show_all) {
    foreach(keys %$csfy) {
        delete $csfy->{$_} unless(%{$csfy->{$_}{hilight}});
    }
}
print("Showing ", scalar keys %$csfy, " out of ", scalar @pkglist,
    " packages\n");
foreach my $pkg (sort keys %$csfy) {
    my %data = %{$csfy->{$pkg}};
    print "$pkg:";
    if($pkg ne $data{svn_path}) {
        print " (SVN: $data{svn_path})";
    }
    print " ", $data{svn}{short_descr} if($data{svn}{short_descr});
    print "\n";
    if(%{$data{status}}) {
        print " - Problems: ", join(", ", keys %{$data{status}}), "\n";
    }
    if(@{$data{notes}}) {
        print " - Notes: ", join(", ", @{$data{notes}}), "\n";
    }
    print " - Repository status: ";
    if($data{hilight}{svn}) {
        print join(", ", keys %{$data{hilight}{svn}}), "\n";
    } else {
        print "OK\n";
    }
    if($data{svn}{version}) {
        print "   + Latest released: $data{svn}{version} ";
        print "($data{svn}{changer})\n";
    }
    if($data{svn}{un_version}) {
        print "   + Latest unreleased: $data{svn}{un_version}\n";
    }
    #
    print " - Debian archive status: ";
    if($data{hilight}{archive}) {
        print join(", ", keys %{$data{hilight}{archive}}), "\n";
    } else {
        print "OK\n";
    }
    if($data{archive}{most_recent}) {
        print "   + Latest version: $data{archive}{most_recent} ";
        print "(from $data{archive}{most_recent_src})\n";
    }
    #
    print " - BTS status: ";
    if($data{hilight}{bts}) {
        print join(", ", keys %{$data{hilight}{bts}}), "\n";
    } else {
        print "OK\n";
    }
    foreach(keys %{$data{bts}}) {
        print "    + Bug #$_ - $data{bts}{$_}{subject}\n";
    }
    #
    print " - Upstream status: ";
    if($data{hilight}{upstream}) {
        print join(", ", keys %{$data{hilight}{upstream}}), "\n";
    } else {
        print "OK\n";
    }
    print "   + URL: $data{upstream_url}\n" if($data{upstream_url});
    if($data{watch}{upstream_version}) {
        print "   + Latest version: $data{watch}{upstream_version}\n";
    }
}
#use Data::Dumper; print Dumper $data;

sub help {
    print <<END;
Usage:
 $0 [options] [dirname [dirname ...]]

 For each named directory, updates the databases with information retrieved
 from the Debian archive, BTS, watchfiles and the Subversion repository.

Options:
 --help, -h         This help.
 --conf, -c FILE    Specifies a configuration file, uses defaults if not
                    present.
 --directories      Treat the parameters as repository directory names, instead
                    of source package names.
 --showall          Show status of all packages, including OK packages.

END
    exit 0;
}
