#!/usr/bin/perl
# vim:ts=4:sw=4:et:ai:sts=4
# $Id: qareport.cgi 13063 2008-01-21 03:01:53Z tincho-guest $
#
# Report packages version states
#
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Copyright Damyan Ivanov <dmn@debian.org>, 2007
# Released under the terms of the GNU GPL 2
use strict;
use warnings;

use DebianQA::Cache;
use DebianQA::Classification;
use DebianQA::Config qw(read_config %CFG);
use DebianQA::Svn;
use CGI ();
use CGI::Carp qw(fatalsToBrowser);
use POSIX qw(locale_h);
use Template ();
use Date::Parse ();

read_config();

my $cgi = new CGI;

my $cache = read_cache(consolidated => "");
my $script_date = '$Date: 2008-01-21 04:01:53 +0100 (lun, 21 gen 2008) $';
$script_date = join( ' ', (split(/ /, $script_date))[1..3] );
my @modified = sort(
    map(
        {
            # Each key of the consolidated cache works like a root cache
            find_stamp($cache->{$_}, "")
        } qw(svn watch archive bts pkglist),
    ),
    Date::Parse::str2time($script_date),
);
my $last_modified = $modified[-1];
my $ims;
my @pkglist = get_pkglist();
my $cls = classify(@pkglist);

my( @no_prob, @for_upload, @for_upgrade, @weird, @waiting, @wip, @with_bugs,
    @all );

unless($cgi->param("show_all"))
{
    foreach(keys %$cls)
    {
        delete $cls->{$_} unless(%{$cls->{$_}{hilight}});
    }
}

foreach my $pkg (sort keys %$cls)
{
    my $data = $cls->{$pkg};

    my $dest;   # like "destiny" :)
    my %info = (
        name => $pkg,
        map(
            ($_=>$data->{$_}),
            qw( watch archive svn bts notes hilight ),
        ),
    );
    my $status = $data->{status};   # to save some typing

    $dest ||= \@for_upgrade if $status->{needs_upgrade};
    $dest ||= \@wip if $status->{not_finished} or $status->{invalid_svn_version};
    $dest ||= \@for_upload if $status->{needs_upload} or $status->{never_uploaded};
    $dest ||= \@weird if $status->{repo_ancient} or $status->{svn_ancient}
        or $status->{upstream_ancient};
    $dest ||= \@wip if $status->{watch_error};
    $dest ||= \@waiting if $status->{archive_waiting};
    $dest ||= \@with_bugs if $status->{has_bugs};
    $dest ||= \@no_prob;

    push @$dest, \%info;
    push @all, \%info;
}

if( $ENV{GATEWAY_INTERFACE} )
{
    my $htmlp = $cgi->Accept("text/html");
    my $xhtmlp = $cgi->Accept("application/xhtml+xml");

    $ims = $cgi->http('If-Modified-Since');
    $ims = Date::Parse::str2time($ims) if $ims;

    if( $ims and $ims >= $last_modified )
    {
        print $cgi->header('text/html', '304 Not modified');
        exit 0;
    }

    my $old_locale = setlocale(LC_TIME);
    setlocale(LC_TIME, "C");
    print $cgi->header(
        -content_type   => (
                ($xhtmlp and $xhtmlp > $htmlp)
                ? 'application/xhtml+xml; charset=utf-8'
                : 'text/html; charset=utf-8'
            ),
        -last_modified   => POSIX::strftime(
            "%a, %d %b %Y %T %Z",
            gmtime($last_modified),
        ),
        $cgi->param("refresh") ? (-refresh => $cgi->param("refresh")) : (),
    );
    setlocale(LC_TIME, $old_locale);
}

my $template = $cgi->param("template") || $CFG{qareport_cgi}{default_template};
my $tt = new Template(
    {
        INCLUDE_PATH => $CFG{qareport_cgi}{templates_path},
        INTERPOLATE  => 1,
        POST_CHOMP   => 1,
        FILTERS      => {
            'quotemeta' => sub { quotemeta(shift) },
        },
    }
);

$tt->process(
    $template,
    {
        data        => $cls,
        group_name  => $CFG{qareport_cgi}{group_name},
        group_url   => $CFG{qareport_cgi}{group_url},
        wsvn_url    => $CFG{qareport_cgi}{wsvn_url},
        (
            ( ($cgi->param('format')||'') eq 'list' )
            ? (
                all => \@all
            )
            : (
                all         => \@no_prob,
                for_upgrade => \@for_upgrade,
                weird       => \@weird,
                for_upload  => \@for_upload,
                waiting     => \@waiting,
                wip         => \@wip,
                with_bugs   => \@with_bugs,
            )
        ),
        shown_packages  => scalar(@all),
        total_packages  => scalar(@pkglist),
#        params          => scalar($cgi->Vars()),
    },
) || die $tt->error;

exit 0;

