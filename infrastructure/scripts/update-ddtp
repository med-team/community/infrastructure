#!/usr/bin/python

#
# This Python script is:
#  (C) 2007, David Paleino <d.paleino@gmail.com>
#  (C) 2008, David Paleino <d.paleino@gmail.com>, Andreas Tille <tille@debian.org>
#
# It is licensed under the terms of GNU General Public License (GPL)
# v3, or any later revision.
#

from urllib import urlopen, urlretrieve
import re
import HTMLTemplate
from datetime import datetime
from email.Utils import formatdate
import time
from sys import argv, exit, stderr

from cddtasktools import CddDependencies, HTMLBASE, REPOS
from Tools import grep

CDD='debian-med'

base = "/var/lib/gforge/chroot/home/groups/debian-med"
#base = "/home/neo/tmp/debmed"
ddtp_url = "http://ddtp.debian.net/debian/dists/sid/main/i18n/Translation-%s"
fetch_url = "http://ddtp.debian.net/ddtss/index.cgi/%s/fetch?package=%s"
trans_url = "http://ddtp.debian.net/ddtss/index.cgi//%s/translate/%s"

# Should we dinamically get this list from PO translations
# of the website?
langs = ["de", "fr", "it", "pt_BR", "es", "ja"]
langs.sort()

dict = {}
longs = {}
shorts = {}

def force_fetch(packages, lang = langs):
	global langs
	
	# If it's a single language to be updated...
	#
	# NOT USED CURRENTLY.
	if type(lang) == "str":
		for name in packages:
			urlopen(fetch_url % (lang, name))
	else:
		for lang_name in lang:
			for pkg_name in packages:
				#print "Fetching %s - %s" % (lang_name, pkg_name)
				urlopen(fetch_url % (lang_name, pkg_name))

def get_status(package):
	global dict
	global langs
	status = {}
	
	for lang in langs:
#		print "Parsing %s - %s" % (package, lang)
		if grep("Package: %s\n" % package, "%s/data/ddtp/Translation-%s" % (base, lang)):
			status[lang] = 1
		else:
			status[lang] = 0
	
	dict[package] = status

def parse_description(package):
	global dict
	global longs
	global shorts
	
	tmp_long = {}
	tmp_short = {}
	for lang in langs:
		if dict[package][lang]:
			f = open("%s/data/ddtp/Translation-%s" % (base, lang), "r")
			# Package: 9wm\nDescription-md5: (?P<md5sum>.{32})\nDescription-\w+: (?P<short>.*)\n(?P<long>.*\n)\n
			# TODO: Fix the regex. (it worked in Kodos :()
			regex = r"""Package: %s
Description-md5: (?P<md5sum>.{32})
Description-\w+: (?P<short>.*)
(?P<long>(^ .*$\n)+\n?)""" % re.escape(package)

			p = re.compile(regex, re.MULTILINE)
			m = p.search(f.read())
			if m:
				tmp_long[lang] = m.group("long").replace("\n ", "<br />").replace("<br />.<br />", "<br /><br />")
				tmp_short[lang] = m.group("short")
			f.close()
		
	longs[package] = tmp_long
	shorts[package] = tmp_short
	
def renderTemplate(node, langs, statuses):
	node.langs.repeat(renderLangs, langs)
	
	names = statuses.keys()
	names.sort()
	
	node.packages.repeat(renderStatuses, names, statuses)
	
	t = datetime.now()
	node.date.content = formatdate(time.mktime(t.timetuple()))

def renderLangs(node, lang):
	node.code.raw = "<img src=\"/img/langs/%s.png\" alt=\"%s\" />" % (lang, lang)

def renderStatuses(node, package, status):
	node.name.raw = '<a href="/ddtp/%s.php">%s</a>' % (package, package)
	
	langs = dict[package].keys()
	langs.sort()

	node.translations.repeat(renderTrans, langs, package, dict[package])

def renderTrans(node, lang, package, status):
	if status[lang] == 0:
		node.status.raw = '<a href="%s"><img src="/img/no.png" alt="no" title="%s - <?=_("translation not available")?>" /></a>' % (trans_url % (lang, package), lang)
	else:
		node.status.raw = '<a href="%s"><img src="/img/ok.png" alt="<?=_("yes")?>" title="%s - <?=_("translated")?>" /></a><a href="%s"><img src="/img/go.png" alt="<?=_("edit")?>" title="%s - <?=_("edit translation")?>" /></a>' % ("/ddtp/%s.php" % package, lang, trans_url % (lang, package), lang)

#tem:
#	con:package
#	rep:langs
#		con:name
#		con:short
#		con:long                                                               
def renderStatic(node, package):
	node.package.content = package
	node.langs.repeat(renderSingleTrans, dict[package], package)
	
	t = datetime.now()
	node.date.content = formatdate(time.mktime(t.timetuple()))

def renderSingleTrans(node, lang, package):
	node.name.raw = '<img src="/img/langs/%s.png" alt="%s" title="%s"/>' % (lang, lang, lang)
	if dict[package][lang]:
		#print "Lang: %s" % lang
		#print "Package: %s" % package
		#print "Dict: %s" % dict[package][lang]
		#print "Short: %s" % shorts[package]
		#print "Long: %s" % longs[package]
		try:
		    node.short.raw = shorts[package][lang]
		    node.long.raw = longs[package][lang]
		except:
		    print "Language %s for package %s not available." % (lang, package)
	else:
		node.short.raw = '<?=_("untranslated")?>'
		node.long.raw = '<?=_("untranslated")?><br /><?=_("Please follow the link below to start translating")?>:<br /><br /><a href="%s">%s</a>' % (trans_url % (lang, package), trans_url % (lang, package))
	
for lang in langs:
	url = ddtp_url % lang
	pos = url.rfind("/")
	name = "%s/data/ddtp/%s" % (base, url[pos + 1:])
	urlretrieve(url, name)

if len(argv) <= 1 :
	print >>stderr, "Warning: No CDD name given as command line argument.  Using %s." % CDD
else:
	if argv[1] not in REPOS.keys():
		print >>stderr, "Unknown CDD name: %s. Don't know what to do." % CDD
		exit(-1)
	CDD = argv[1]

cdeps=CddDependencies(CDD)
cdeps.GetAllDependencies()

packages = cdeps.GetAllDependantPackagesOfCDD()

# XXX TODO HACK NEWS
# Re-enable this to force-fetch the packages.
# Shouldn't be done unless having talked to DDTSS admins.
#  -- David
#force_fetch(packages)
for item in packages:
	get_status(item)
	parse_description(item)

#print dict
#print longs
#print shorts

# Let's generate the overview page
f = open("%s/htdocs/ddtp.tmpl" % base)
tmpl = HTMLTemplate.Template(renderTemplate, f.read())
f.close()

f = open("%s/static/ddtp.php" % base, "w")
f.write(tmpl.render(langs, dict))
f.close()

# Let's generate nice static overview per-package pages
for name in packages:
	outfile = "%s/static/ddtp/%s.php" % (base, name)
	f = open("%s/htdocs/ddtp_details.tmpl" % base)
	tmpl = HTMLTemplate.Template(renderStatic, f.read())
	f.close()
	
#	print tmpl.structure()
	
	f = open(outfile, "w")
	f.write(tmpl.render(name))
	f.close()
