#!/bin/sh

#just a test... maybe it's just website/ which doesn't trigger post-commit

if [ ! -d "debian" ]; then
	echo This script should be execute just atop of a Debian directory if a subversion repository
	exit
fi

if [ ! -d ".svn" ]; then
	echo This directory was not checked out from a subversion reporitory
	exit
fi

origDir=""

if [ -r ".svn/deb-layout" ]; then
	echo "Found .svn/deb-layout"
	origDir=`grep ^origDir .svn/deb-layout | cut -f2 -d= | tr -d "\n"`
	echo "origDir set to '$origDir'"
fi

if [ -z "$origDir" ]; then
	origDirDefault="../tarballs"
	origDirPath="$origDirDefault ../tarballs ../../tarballs"

	for p in $origDirPath
	do
		if [ -d $p ]; then
			echo found dir $p
			origDir=$p
			break
		else
			echo No found: $p
		fi
	done

	if [ -z "$origDir" ]; then
		origDir=$origDirDefault
	fi

	echo "Creating entry for origDir in .svn/deb-layout"
	echo "origDir=$origDir" >> .svn/deb-layout
fi


if [ ! -d $origDir ]; then
	echo Creating directory $origDir
	mkdir $origDir || (
		echo Could not create dir $origDir
		exit
	)
fi

d=`pwd`

echo Performing call to uscan
uscan.pl  --destdir=$origDir --force-download --verbose $d
