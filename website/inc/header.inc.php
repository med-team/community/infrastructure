<?php
	require_once("locale.inc.php");

	$type = "text/html; charset=UTF-8";
	$xml = "";
		
	if (isset($_SERVER['HTTP_ACCEPT'])) {
		if (stristr($_SERVER['HTTP_ACCEPT'], "application/xhtml+xml")) {
			$type = "application/xhtml+xml; charset=UTF-8";
			$xml = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
		} else {
			$type = "text/html; charset=UTF-8";
			$xml = '';
		}
	}

	header("content-type: $type");

?><?=$xml?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=$lang?>">
<head>
<title>Debian-Med Project</title>
<meta http-equiv="Content-Type" content="<?=$type?>" />
<link href="/inc/style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div style="text-align: center;">
<a href="http://debian-med.alioth.debian.org/">
	<img src="/img/logo.png" alt="Debian Med Project" height="93" width="446" />
</a>
</div>
<div class="heading">
	<div class="tabBar" style="text-align: center;">
		<?= sprintf(_("Help us to see Debian used by medical practicioners and researchers! Join us on the %sAlioth page%s."),
				'<a href="http://alioth.debian.org/projects/debian-med">', '</a>'); ?>
	</div>
	<?php
		/* if ($show_locale_warning && ($_SERVER["PHP_SELF"] != "/locales.php")) {
	<div class="tabError">
		<p class="error">
			<div class="floatLeft">
				<img src="/img/warning.png" />
			</div>
			<?=_("You're using a low priority locale.<br />Please ask the site administrators to add your locale, or provide one yourself :).")?><br />
			<?= sprintf(_("Visit the %sLocalization page%s.), '<a href="/locales.php">', "</a>"); ?>
		</p>
	</div>
		}*/
	?>
</div>
