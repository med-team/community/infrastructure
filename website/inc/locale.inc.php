<?php
// _SERVER["HTTP_ACCEPT_LANGUAGE"] it-it,it;q=0.8,en-gb;q=0.6,en-us;q=0.4,en;q=0.2

// this odd name is to avoid conflict with
// po-stats.inc.php's clean()
function __clean__($item) {
	if (is_array($item)) {
		if (!count($item))
			$item = null;
		else {
			foreach ($item as $key => $value) {
				$item[$key] = __clean__($value);
				if (empty($item[$key]))
					unset($item[$key]);
			}
		}
	} else {
		if (empty($item))
			$item = null;
	}
	return $item;
}

$languages = $_SERVER["HTTP_ACCEPT_LANGUAGE"];
$lang_arr = explode(",", $languages);
$lang_arr[0] .= ";q=1.0";

$def_locale = "en_US";
$priority = "0.0";

$i = 0;
foreach ($lang_arr as $lang_code) {
	$tmp[] = explode(";", $lang_code);
	
	// let's use the aa_BB format
	preg_match("/([a-zA-Z]+)-([a-zA-Z]+)/", $tmp[$i][0], $matches);
	$tmp[$i][0] = ($matches) ? strtolower($matches[1]) . "_" . strtoupper($matches[2]) : $tmp[$i][0];
	$langs[str_replace("q=", "", $tmp[$i][1])] = $tmp[$i][0];
	$i++;
}

// let's sort them by priority, highest first
krsort($langs);

//if ($fp = fopen("/var/lib/gforge/chroot/home/groups/debian-med/htdocs/inc/locales.txt", "r")) {
if ($fp = fopen(getcwd() . "/inc/locales.txt", "r")) {
	while (!feof($fp)) {
		$raw .= fgets($fp, 4096);
	}
	fclose($fp);
} else {
	die ("Please contact the site administrators: debian-med@lists.debian.org -- Cannot open locales.txt");
}

// let's remove comments and empty lines
$raw = preg_replace("/^\\#.*\\n/m", "", $raw);
$raw = preg_replace("/^[ ]*\\n/m", "", $raw);

$tmp_installed = __clean__(explode("\n", $raw));
foreach ($tmp_installed as $pair) {
	$row = explode("\t", $pair);
	$installed[$row[0]] = $row[1];
}

foreach ($langs as $index => $language) {
	if ($installed[$language]) {
		$locale = $installed[$language];
		$priority = $index;
		break;
	} else {
		$locale = $def_locale;
	}
}

// we'll use this in a <meta> tag in our header
// and in the xml declaration.
preg_match("/([a-z]+)_[A-Z]+/", $locale, $matches);
$lang = $matches[1];

if ($priority < "0.8") {
	$show_locale_warning = true;
}

putenv("LC_ALL=$locale.UTF-8");
putenv("LANG=$locale.UTF-8");
putenv("LANGUAGE=$locale.UTF-8");
setlocale(LC_ALL, "$locale.UTF-8");
$domain = "messages";
bindtextdomain($domain, str_replace($_SERVER["SCRIPT_NAME"], "", $_SERVER["SCRIPT_FILENAME"]) . "/locale");
textdomain($domain);

?>
