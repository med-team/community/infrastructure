###
# PLEASE, DON'T MANUALLY EDIT THIS FILE.
# OTHERWISE YOU MIGHT BREAK UP LOCALIZATION.
###

###
# The format of this file is:
#
# alias<TAB>locale
#
# For example, the "it" locale points to the real locale
# on the server, it_IT.
###

# Italian
it	it_IT
it_CH	it_IT
it_IT	it_IT

# German
de	de_DE
de_DE	de_DE
de_CH	de_DE
de_BE	de_DE
de_AT	de_DE
de_LU	de_DE

# French
fr	fr_FR
fr_FR	fr_FR
fr_BE	fr_FR
fr_CA	fr_FR
fr_CH	fr_FR
fr_LU	fr_FR

# English
en	en_US
en_US	en_US
en_GB	en_US
en_AU	en_US
en_BW	en_US
en_CA	en_US
en_DK	en_US
en_HK	en_US
en_IN	en_US
en_NZ	en_US
en_IE	en_US
en_PH	en_US
en_SG	en_US
en_ZA	en_US
en_ZW	en_US

# Portuguese
pt	pt_BR
pt_BR	pt_BR
pt_PT	pt_BR

#Russian
ru	ru_RU