<?php

require_once("table-parser.inc.php");

function retrieveData($url) {
	$fp = fopen($url, "r");
	if ($fp) {
		while (!feof($fp)) {
			$data .= fgets($fp, 4096);
		}
		fclose($fp);
	} else {
		echo "Cannot open $url.\n";
	}
	
	return $data;
}

function ParseMembersTable($project_id) {
	$url = "http://alioth.debian.org/project/memberlist.php?group_id=".$project_id;
	
	$page = retrieveData($url);

	/* We now need to extract our table from the mess of the page.
	 * We're looking for:
	 *     <table width="100%" border="0" cellspacing="1" cellpadding="2">
	 * since it is the only occurrence on the page.
	 */

	$needle = '<table width="100%" border="0" cellspacing="1" cellpadding="2">';
	$toend = strstr($page, $needle);

	// In PHP6, we would do this:
	//$needle = '</table>';
	//$table = strstr($toend, $needle, true);

	$needle = "</table>";
	$offset = strpos($toend, $needle);
	$table = substr($toend, 0, $offset + strlen($needle));

	$arr = TableParser::Go($table);
	array_shift($arr);
	
	$members = array();
	foreach ($arr as $person) {
		$name = ($person[0]) ? $person[0] : $person[1]; // take the name. Admins have index 1, Devs 0.
		$name = trim($name);
		foreach ($person as $value) {
			if ($person[2]) { // if index 2 is present, then it's not an admin -- fix indexes properly
				$members[$name]["userid"] = $person[2];
				$members[$name]["role"] = "developer";
			} else {
				$members[$name]["userid"] = $person[3];
				$members[$name]["role"] = "admin";
			}
		}
	}
	return $members;	
}
?>
