<?php

function getCleanPO($po) {
	global $author, $team;
	
	$fp = fopen($po, "r");
	if ($fp) {
		while (!feof($fp)) {
			$data .= fgets($fp, 4096);
		}
		fclose($fp);
	}

	$regex = "/^[\t ]*\"Last-Translator: (\w+.*<.*@.*>)/m";
	preg_match($regex, $data, $matches);
	$author = $matches[1];
	
	$regex = "/^[\t ]*\"Language-Team: (\w+.*<.*@.*>)/m";
	preg_match($regex, $data, $matches);
	$team = $matches[1];

	
	$arr = explode("#:", $data);
	array_shift($arr);

	for ($i = 0; $i < count($arr); $i++) {
		$regex = "/^\\#.*\\n/m";
		$arr[$i] = preg_replace($regex, "", $arr[$i]);

		$regex = "/.*\.php:\d+.*\n/";
		$arr[$i] = preg_replace($regex, "", $arr[$i]);

		$regex = "/\n$/";
		$arr[$i] = preg_replace($regex, "", $arr[$i]);
		
		$arr[$i] = explode("\n", $arr[$i]);

		for ($k = 0; $k < count($arr[$i]); $k++) {
			preg_match("/(\w+)[\t ]+\"/", $arr[$i][$k], $matches);
		
			if (($matches[1] != "msgid" && $matches[1] != "msgstr") || !$matches[1]) {
				$regex = "/(\w+.*\"\")/";
				preg_match($regex, $arr[$i][$k - 1], $submatch);
				if ($submatch[1]) {
					$arr[$i][$k - 1] = substr($submatch[1], 0, strlen($submatch[1]) - 1)."foo\"";
				} else {
					$arr[$i][$k] = null;
				}
			}
		}
	}

	return clean($arr);
}

function clean($item) {
	if (is_array ($item)) {
		if (!count($item)) {
			$item = null;
		} else {
			foreach ($item as $key => $value) {
				$item[$key] = clean($value);
				if (empty($item[$key]))
					unset($item[$key]);
			}
		}
	} else {
		if (empty($item))
			$item = null;
		else {
			$regex = "/(\w+).*\"/";
			preg_match($regex, $item, $matches);
			if ($matches[1] != "msgid" && $matches[1] != "msgstr")
				$item = null;
		}
	}
	
	return $item;
}

function makePOStats($po) {
	global $author, $team;
	
	$pairs = getCleanPO($po);

	$count = 0;
	$untranslated = 0;
	$empty_msgid = false;
	
	foreach($pairs as $item) {
		$regex = "/(\w+).*\"(.*)\"/";
	
		sort($item);
		
		preg_match($regex, $item[0], $matches);
		if (!trim($matches[2])) {
			// this is just a fallback
			$empty_msgid = true;
		}
		
		preg_match($regex, $item[1], $matches);
		if (!$empty_msgid && !trim($matches[2])) {
			$untranslated++;
		}
		
		$empty_msgid = false;		
		$count++;
	}
	
	return array("count" => $count,
				"missing" => $untranslated,
				"complete" => 100 - (($untranslated / $count) * 100),
				"author" => $author,
				"team" => $team,
				);
}
?>
