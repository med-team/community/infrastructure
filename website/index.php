<?php
	require_once("inc/header.inc.php");
?>
<table class="columns">
<tr>
	<td class="left">
		<span class="section"><?=_("information")?></span>
		<div class="section">
			<div class="sectionTop"></div>
			<div class="row" style="text-align: center;">
				<?=_("Developers please visit our")?><br />
				<a href="http://wiki.debian.org/DebianMed"><?=_("Wiki page")?></a>
			</div>
			<div class="row">
				<?=_("The Debian Med project presents packages that are associated with <ul><li>medicine</li><li>pre-clinical research</li><li>life science.</li></ul> Its developments are mostly focused on three areas for the moment: <ul><li>medical practice</li><li>imaging</li><li>bioinformatics</li></ul>and can be installed directly from every Debian installation.")?>
			</div>
		<?php
			if ($show_locale_warning) {
		?>
			<div class="row">
				<p class="error">
					<img src="/img/warning.png" alt="<?=_("warning")?>" style="text-align:center;" />
					<br />
					<?= sprintf(_("Your browser uses language settings that we could not yet provide translations for.<br />".
							"If you can spare one to two hours then please consider to help us in translating our pages ".
							"for your people, too. Instructions are found %shere%s."),
							'<a href="/locales.php">', '</a>');?>
					<br /><br />
					<?= sprintf(_("Visit the %sLocalization page%s."), '<a href="/locales.php">', '</a>'); ?>
				</p>
			</div>
		<?php
			}
		?>
		</div>
		<span class="section"><?=_("pages")?></span>
		<div class="section">
			<div class="sectionTop"></div>
			<div class="row">
				<ul>
					<li><a href="http://blends.debian.org/med/tasks/index"><?=_("Tasks of our Blend")?></a></li>
					<li><a href="http://blends.debian.org/med/bugs/"><?=_("Bugs")?></a></li>
					<li><a href="http://blends.debian.org/med/thermometer/"><?=_("Thermometer")?></a></li>
					<li><a href="/docs/policy.html"><?=_("Group policy")?></a></li>
					<li><a href="/cgi-bin/qareport.cgi"><?=_("Quality Assurance")?></a></li>
	<!--				<li><a href="/ddtp.php"><?=_("Debian Description Translation Project")?></a></li>	NEEDS FIXING FIRST BEFORE WE CAN LINK TO IT		-->
					<li><a href="http://svn.debian.org/wsvn/debian-med/trunk/"><?=_("SVN repository")?></a></li>
					<li><a href="http://qa.debian.org/developer.php?login=debian-med-packaging@lists.alioth.debian.org&amp;ordering=3"><?=_("Packages Overview")?></a></li>
				</ul>
				<hr />
				<ul>
					<li><a href="/locales.php"><?=_("Localizations")?></a></li>
					<li><a href="/projects.xml"><?=_("Projects")?></a></li>
					<li><a href="/support.xml"><?=_("Support")?></a></li>
					<li><a href="/organisations.xml"><?=_("Organisations")?></a></li>
				</ul>
			</div>
		</div>
		<span class="section"><?=_("members")?></span>
		<div class="section">
			<div class="sectionTop"></div>
			<div class="row">
				<div class="relatedHeading">
					<table class="related">
					<?php
						require_once("inc/parser.inc.php");
						
						$members = ParseMembersTable(30063);
						
						ksort($members);
						
						$img = "";
						foreach ($members as $name => $details) {
							switch ($details["role"]) {
								case "admin":
									$img = "/img/wh_green.png";
									$alt = _("Project Administrator");
									break;
								case "developer":
								default:
									$img = "/img/wh_grey.png";
									$alt = _("Project Developer");
							}
							?>
					<tr><td>
						<img src="<?=$img?>" alt="<?=$alt?>" title="<?=$alt?>" height="12" width="12" />
						<a href="http://alioth.debian.org/users/<?=$details["userid"]?>"><?=$name?></a>
					</td></tr>
							<?php
						}
					?>
					</table>
				</div>
				<table class="related">
				<tr><td>
					<img src="/img/wh_green.png" alt="<?=_("Green Wheel")?>" height="12" width="12" />
					<?=_("Project Administrator")?>
				</td></tr>
				<tr><td>
					<img src="/img/wh_grey.png" alt="<?=_("Grey Wheel")?>" height="12" width="12" />
					<?=_("Project Developer")?>
				</td></tr>
				</table>
			</div>
		</div>
		<span class="section"><?=_("UTC time")?></span>
		<div class="section">
			<div class="sectionTop"></div>
			<div class="row"><?=date("r", time())?></div>
			<div class="row"><?=strftime("%c", time())?></div>
		</div>
		<span class="section"><?=_("badges")?></span>
		<div class="section">
			<div class="sectionTop"></div>
			<div class="row">
				<!-- We still need to fix some errors to validate the document as XHTML
				<p style="text-align: center">
					<a href="http://validator.w3.org/check?uri=referer">
						<img src="http://www.w3.org/Icons/valid-xhtml11-blue" alt="<?=_("Valid XHTML 1.1")?>" height="31" width="88" />
					</a>
				</p>
				-->
				<!-- moz-opacity nor filter:alpha are valid CSS
				<p style="text-align: center">
					<a href="http://jigsaw.w3.org/css-validator/validator?uri=http://debian-med.alioth.debian.org/">
						<img src="http://www.w3.org/Icons/valid-css2-blue" alt="<?=_("Valid CSS 2")?>" height="31" width="88" />
					</a>
				</p>
				-->
				<!-- I am not so very serious about the BOINC link, it is just that the 
				     validator link alone was rather ... boring? -->
				<p style="text-align: center">
					<a href="http://boinc.berkeley.edu">
						<img src="http://boinc.berkeley.edu/logo/www_logo.gif" alt="<?=_("Berkeley Open Infrastructure for Network Computing")?>" height="73" width="164" />
					</a>
				</p>
			</div>
		</div>
	</td>
	<td class="main">
		<span class="section"><?=_("recent activity")?></span>
		<div class="section">
			<div class="sectionTop"></div>
			<div class="row">
				<table width="100%">
				<tr>
					<th><?=_("date")?></th>
					<th><?=_("author")?></th>
					<th style="width:400px"><?=_("content")?></th>
					<th><?=_("link")?></th>
				</tr>
				<?php
					include_once("inc/lastRSS.php");
					$rss = new lastRSS;
					$rss->cache_dir = "./cache";
					$rss->cache_time = 3600;
					
					if ($rs = $rss->get("http://cia.vc/stats/project/Debian-Med/.rss")) {
						// we show only the last 10 commits by default
						for ($i = 0; $i < 9; $i++) {
							$desc = html_entity_decode($rs["items"][$i]["description"]);
							preg_match("/(Commit by.*)<strong>(.*)<\/strong>/", $desc, $matches);
							$author = $matches[2];
							$desc = preg_replace("/(Commit by.*<strong>.*<\/strong>.*<span.*::.*<\/span>)/", "", $desc);
				?>
				<tr>
					<td><?=strftime("%c", strtotime($rs["items"][$i]["pubDate"]))?></td>
					<td><a href="http://alioth.debian.org/users/<?=$author?>"><?=$author?></a></td>
					<td><?=$desc?></td>
					<td><a href="<?=$rs["items"][$i]["link"]?>">&#187;</a></td>
				</tr>
						<?php
						}
					}
				?>
				</table>
			</div>
		</div>
		<span class="section"><?=_("todo")?></span>
		<div class="section">
			<div class="sectionTop"></div>
			<div class="row">
				<ul>
					<li>use AJAX to make the commit feed "live";</li>
					<li>add Packages information:
						<ul>
							<li>upload status;</li>
							<li>buildd status;</li>
						</ul></li>
					<li>use a general backend for retrieving information (i.e. do not put retrieving code directly 
					into index.php);</li>
					<li>sync <a href="http://www.medfloss.org/node/350">projects missing in Debian from MedFLOSS</a> with our tasks list</li>
					<li>localization support:
						<ul>
							<li>automatize .po generation (and compilation);</li>
						</ul></li>
					<li>make this TO-DO list dynamic;</li>
					<li>add parsing scripts under SVN control;</li>
					<li>add a "BuildD page" parsing <a href="http://people.debian.org/~igloo/status.php">http://people.debian.org/~igloo/status.php</a>;</li>
				</ul>
			</div>
		</div>
	</td>
</tr>
</table>
<hr />
<p>
<small><?=_("Please, note that this is a SVN export of our website. It might break during SVN commits.")?></small>
</p>
<?php
	require_once("inc/footer.inc.php")
?>
