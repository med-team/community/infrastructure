��    2      �  C   <      H     I     X     g     �  &   �     �     �  
   �     �  d   �     _     k  p   y     �     �  V   
  Y   a     �     �     �     �     �               *     2  Z  E     �     �     �     �      �  	   �  �   	     �	     �	     �	     
     
     
     
     
     ,
     4
     :
     J
     R
     W
     j
  {  r
  '   �          $     ?  5   X      �     �  
   �     �  ~   �  "   _     �  �   �     #     2  t   J  o   �  	   /     9     G  	   U     _     {     �     �     �  �  �     g     s     �     �  3   �     �                	   $     .     4     G     N     b     v          �     �     �     �     �         !   ,             
      "   1               -      (         #          %   .          +      0       *       )                    $                    2                              /             &         	            '           Add new locale Current locale Currently installed locales DDTP Statistics Debian Description Translation Project Developers please visit our Green Wheel Grey Wheel Group policy Help us to see Debian used by medical practicioners and researchers! Join us on the %sAlioth page%s. Last update Localizations More information on how to contribute to the Debian Med project, can be found in the %sHow to Contribute%s page. Organisations Packages Overview Please note: this page gets automatically updated twice a day, on 00:00 and 12:00 UTC. Please, note that this is a SVN export of our website. It might break during SVN commits. Priority Project Administrator Project Developer Projects Quality Assurance SVN repository Summary for package Support Tasks of our Blend The Debian Med project presents packages that are associated with <ul><li>medicine</li><li>pre-clinical research</li><li>life science.</li></ul> Its developments are mostly focused on three areas for the moment: <ul><li>medical practice</li><li>imaging</li><li>bioinformatics</li></ul>and can be installed directly from every Debian installation. UTC time Updating locales Valid CSS 2 Valid XHTML 1.1 Visit the %sLocalization page%s. Wiki page Your browser uses language settings that we could not yet provide translations for.<br />If you can spare one to two hours then please consider to help us in translating our pages for your people, too. Instructions are found %shere%s. author badges content date information link locale localization members pages recent activity summary team translation status warning Project-Id-Version: Debian Med Website
Report-Msgid-Bugs-To: debian-med@lists.debian.org
POT-Creation-Date: 2010-08-23 11:51+0200
PO-Revision-Date: 2011-08-23 00:37+0200
Last-Translator: Luis Rivas Va&ntilde;&oacute; <luinix@gmail.com>
Language-Team: espa&ntilde;ol <es@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 A&ntilde;adir nueva localizaci&oacute;n Locale actual Localizaciones disponibles Estad&iacute;sticas DDTP Proyecto de traducci&oacute;n de descripciones Debian Desarrolladores, visiten nuestra Rueda verde Rueda gris Pol&iacute;tica del grupo ¡Ayuda a que Debian sea usada por profesionales e investigadores m&eacute;dicos! &Uacute;nete en la %sp&aacute;gina Alioth%s. &Uacute;ltima actualizaci&oacute;n localizaciones Puede encontrar m&aacute;s informaci&oacute;n sobre c&oacute;mo contribuir al proyecto Debian Med en la p&aacute;gina %sC&oacute;mo contribuir%s. Organizaciones Vistazo de los paquetes Nota: esta p&aacute;gina se actualiza autom&aacute;ticamente dos veces al d&iacute;a, a las 00:00 y a las 12:00 UTC. Por favor, tenga en cuenta que nuestra web es una exportaci&oacute;n SVN. Puede fallar durante los commits SVN. Prioridad Administrador Desarrollador Proyectos Aseguramiento de la Calidad Repositorio SVN Resumen de paquete Soporte Tareas de nuestro Blend El proyecto Debian Med presenta paquetes asociados con <ul><li>medicina</li><li>investigaci&oacute;n cl&iacute;nica</li><li>ciencias biol&oacute;gicas.</li></ul> Sus desarrollos est&aacute;n por el momento enfocados en tres &aacute;reas: <ul><li>pr&aacute;ctica m&eacute;dica</li><li>imagen m&eacute;dica</li><li>bioinform&aacute;tica</li></ul>y puede ser instalado directamente desde cualquier instalaci&oacute;n de Debian. Fecha (UTC) Actualizando localizaciones CSS 2 v&aacute;lido XHTML 1.1 v&aacute;lido Visite la %sp&aacute;gina de localizaci&oacute;n%s. p&aacute;gina Wiki Su navegador usa una configuraci&oacute;n de idioma para la que a&uacute;n no tenemos traducci&oacute;n.<br />Si puede emplear una o dos horas por favor considere ayudarnos traduciendo nuestra p&aacute;gina tambi&eacute;n a su idioma. Cuenta con instrucciones %saqu&iacute;%s. Autor badges Contenido fecha informaci&oacute;n enlace Localizaci&oacute;n localizaci&oacute;n Miembros p&aacute;ginas actividad reciente resumen Equipo Estado de la traducci&oacute;n aviso 