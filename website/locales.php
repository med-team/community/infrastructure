<?php
	require_once("inc/header.inc.php");
?>
<table class="columns">
<tr>
	<td class="left">
		<span class="section"><?=_("summary")?></span>
		<div class="section">
			<div class="sectionTop"></div>
			<div class="row" style="text-align: center;">
				<?=_("Current locale")?>:<br />
				<strong><?=$locale?></strong>
			</div>
			<div class="row" style="text-align: center;">
				<?=_("Priority")?>:<br />
				<strong><?=$priority?></strong>
			</div>
			<?php
				if ($show_locale_warning) {
			?>
			<div class="row">
				<p class="error">
					<p style="text-align: center;">
						<img src="/img/warning.png" />
					</p>
					<?= sprintf(_("Your browser uses language settings that we could not yet provide translations for.<br />".
							"If you can spare one to two hours then please consider to help us in translating our pages ".
							"for your people, too. Instructions are found %shere%s."),
							'<a href="/locales.php">', '</a>'); ?>
					<br /><br />
					<?= sprintf(_("More information on how to contribute to the Debian Med project, can be found in the %sHow to Contribute%s page."),
							'<a href="/contribute.php"><del>', '</del></a>');?>
				</p>
			</div>
			<?php
				}
			?>
		</div>
	</td>
	<td class="main">
		<span class="section"><?=_("localization")?></span>
		<div class="section">
			<div class="sectiontop"></div>
			<div class="row">
				<h1><?=_("Currently installed locales")?></h1>
				<table>
				<tr>
					<th><?=_("locale")?></th>
					<th><?=_("translation status")?></th>
					<th><?=_("author")?></th>
					<th><?=_("team")?></th>
				</tr>
				<tr>
					<td style="text-align: center"><strong>en_US</strong></td>
					<td style="text-align: center"><strong>100%</strong> (builtin)</td>
					<td></td>
					<td></td>
				</tr>
				<?php
					require_once("inc/po-stats.inc.php");
					
					$handle = opendir("locale/");
					$avail_locales = array();
					while ($file = readdir($handle)) {
						if ($file != "." && $file != ".." && $file != ".svn") {
							// we get this $locales_dir from locale.inc.php
							if (is_dir("locale/$file")) {
								$po = "locale/$file/LC_MESSAGES/messages.po";
								$avail_locales[$file] = makePOStats($po);
							}
						}
					}
					
					while (list($code, $lang) = each($avail_locales)) {
						$lang["complete"] = round($lang["complete"], 2);
						if ($lang["complete"] == "100") {
							$lang["complete"] = "<strong>".$lang["complete"]."</strong>";
						}
				?>
				<tr>
					<td style="text-align: center"><strong><?=$code?></strong></td>
					<td style="text-align: center"><?=$lang["complete"]?>%</td>
					<td><?=str_replace(array("<", ">"), array("&lt;", "&gt;"), $lang["author"])?></td>
					<td><?=str_replace(array("<", ">"), array("&lt;", "&gt;") ,$lang["team"])?></td>
				</tr>
				<?php
					}
				?>
				</table>
			</div>
			<div class="row">
				<h1><?=_("Updating locales")?></h1>
				<span class="info">
					<p class="info">
						To update existing locales with new messages from the code, first cd to the locale/ directory:
						<pre>
$ cd trunk/community/website/locale/</pre><br />
						After that, you will find some scripts there. So, run:
						<pre>
$ ./update-messages
$ ./update-locales
......done
...
$ svn ci -m "Catalogs updated"
...
Commit of revision xxx done.
$ </pre><br />
						After that, pick up your favourite editor, edit your .po file, and commit it!
					</p>
				</span>
				<h1><?=_("Add new locale")?></h1>
				<span class="info">
					<p class="info">
						To add a new locale, please use your authenticated SVN access.<br />
						First, checkout the locale/ directory:
						<pre>
$ svn co svn+ssh://myaliothlogin@svn.debian.org/svn/debian-med/trunk/community/website/locale/</pre><br />
						After this, create a new directory for your locale (let's suppose it's fr_FR):
						<pre>
$ mkdir -p fr_FR/LC_MESSAGES/</pre><br />
						Next step is actually adding the .po catalog. Copy the .pot template to your newly created 
						directory, and start translating:
						<pre>
$ cp messages.pot fr_FR/LC_MESSAGES/messages.po
$ cd fr_FR/LC_MESSAGES/
$ vi messages.po</pre><br />
						Now you need to compile the catalog you've just translated, and add everything under SVN 
						control (we're in the LC_MESSAGES/ directory in this example):
						<pre>
$ msgfmt messages.po
$ ls
messages.mo messages.po
$ cd ../../ # up to the locale/ directory
$ ls
it_IT/ fr_FR/ messages.pot
$ svn add fr_FR/
A fr_FR/
A fr_FR/LC_MESSAGES/
A fr_FR/LC_MESSAGES/messages.mo
A fr_FR/LC_MESSAGES/messages.po
$ </pre><br />
						Now it's time to commit the changes:
						<pre>
$ svn commit -m "Adding fr_FR locale"
Sending      locale/fr_FR/
Sending      locale/fr_FR/LC_MESSAGES/
Sending      locale/fr_FR/LC_MESSAGES/messages.mo
Sending      locale/fr_FR/LC_MESSAGES/messages.po
Sending data ....
Commit of Revision xxx done.
$ </pre><br />
						Please note that the <em>automatic loading</em> of locales has not been tested yet. Please 
						send an e-mail to <a href="mailto:debian-med@lists.debian.org">debian-med@lists.debian.org</a>
						if you encounter any problems.
					</p>
				</span>
			</div>
		</div>
	</td>
</tr>
</table>
<?php
	require_once("inc/footer.inc.php");
?>
