<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="ISO-8859-1"/>
<xsl:variable name="empty_string" select="''" />
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Organisations sharing aims with Debian-Med</title>
    <meta http-equiv="Content-Type" content="text/html" />
    <link href="/inc/style.css" type="text/css" rel="stylesheet" />
  </head>
  <body>
    <div style="text-align: center;">
    <a href="http://debian-med.alioth.debian.org/">
	<img src="/img/logo.png" alt="Debian Med Project" />
    </a>
  </div>

<!-- HTML copied from index.php, need to be manually updated -->
<table class="columns">
  <tr>
    <td class="left">
      <span class="section">Information</span>
      <div class="section">
        <div class="sectionTop"></div>
        <div class="row" style="text-align: center;">
          Developers please visit our<br />
            <a href="http://wiki.debian.org/DebianMed">Wiki page</a>
        </div>
        <div class="row">
          The Debian Med project presents packages that are associated with <ul><li>medicine</li><li>pre-clinical research</li><li>life science.</li></ul> Its developments are mostly focused on three areas for the moment: <ul><li>medical practice</li><li>imaging</li><li>bioinformatics</li></ul>and can be installed directly from every Debian installation.
        </div>
      </div>

      <span class="section">Pages</span>
      <div class="section">
        <div class="sectionTop"></div>
        <div class="row">
          <ul>
            <li><a href="http://blends.debian.org/med/tasks">Tasks of our Blend</a></li>
            <li><a href="http://blends.debian.org/med//bugs/">Bugs</a></li>
            <li><a href="http://blends.debian.org/med//thermometer/">Versions overview</a></li>
            <li><a href="/docs/policy.html">Group policy</a></li>
            <li><a href="/cgi-bin/qareport.cgi">Quality Assurance</a></li>
            <li><a href="/ddtp.php">Debian Description Translation Project</a></li>
            <li><a href="http://svn.debian.org/wsvn/debian-med/trunk/">SVN repository</a></li>
            <li><a href="http://qa.debian.org/developer.php?login=debian-med-packaging@lists.alioth.debian.org&amp;ordering=3">Packages Overview</a></li>
            <hr />
            <li><a href="/locales.php">Localizations</a></li>
            <li><a href="projects.xml">Projects</a></li>
            <li><a href="support.xml">Support</a></li>
            <li><a href="organisations.xml">Organisations</a></li>
	  </ul>
	</div>
      </div>
   </td>
   <td class="main" valign="top">

   <!-- Presentation of organisations -->

    <xsl:for-each select="organisations">
     <h1>Organisations sharing aims with Debian Med</h1>
     <hr />
     <table border="0">
    <xsl:for-each select="org">
    <xsl:variable name="url" select="url" />
    <xsl:variable name="longtitle" select="longtitle" />
       <tr>
         <td rowspan="2" align="center" valign="top"><a href="{url}"><xsl:value-of select="@title"/></a></td><td>
	        <xsl:if test="$empty_string!=$longtitle">
			<xsl:value-of select="longtitle"/><br/>
		</xsl:if>
		<xsl:value-of select="description"/>
         </td>
           </tr>
           <tr>
             <td>
               <ul>
                  <li>Outreach: <xsl:value-of select="region"/></li>
                  <li>Members: <xsl:value-of select="members"/></li>
      	          <li>URL: <a href="{url}"><xsl:value-of select="url"/></a></li>
	        </ul>
              </td>
            </tr>
            <tr><td colspan="2"><hr /></td></tr>
    </xsl:for-each>
          </table>
    </xsl:for-each>

   <!-- Presentation of sources for support -->

    <xsl:for-each select="support">
     <h1>Sources for support for packages in Debian Med</h1>
	<p>
	  With Debian, there are several sources for support. Firstly,
	  there is the maintainer of the packages, who can be reached
	  with "<tt>reportbug </tt><i>packagename</i>" from the command
	  line. The maintainer is commonly the Debian Med mailing list,
	  please be aware that your email will be picked up by google
	  and the public may learn about your activities this way.
	</p>
	<p>
	  You may prefer to contact individual contributors to Debian
	  Med directly or ask to be contacted on the mailing list. This
	  way, you can find partners for scientific projects, delegate
	  some analyses, ask for some development or offer internships
	  of various kinds for students. And yes, it is not unlikely
	  you'd be asked to pay for that.
	</p>
	<p>
	  Finally, as a commercial entity you should rest assured
	  that you are not alone in your decision for Debian
	  as a distribution. For a general support of Debian
	  you will find professional contacts in your area on
	  <a href="http://www.debian.org/consultants/">www.debian.org/consultants</a>.
	  For support for our packages Debian Med that demand a rather
	  highly specialised domain knowledge, the following companies
	  came to our attention:
	</p>
     <hr />
     <table border="0">
    <xsl:for-each select="entity">
    <xsl:variable name="url" select="url" />
       <tr>
         <td rowspan="2" align="center" valign="top"><a href="{url}"><xsl:value-of select="@title"/></a></td><td>
		<i><xsl:value-of select="longtitle"/></i>
		<p>
		<xsl:value-of select="description"/>
		</p>
         </td>
           </tr>
           <tr>
             <td>
               <ul>
                  <li>Location: <xsl:value-of select="region"/></li>
      	          <li>URL: <a href="{url}"><xsl:value-of select="url"/></a></li>
	        </ul>
              </td>
            </tr>
            <tr><td colspan="2"><hr /></td></tr>
    </xsl:for-each>
          </table>
    </xsl:for-each>


   <!-- Presentation of sources for projects -->


    <xsl:for-each select="projects">
     <h1>Projects of Debian Med</h1>
	<p>
	  With Debian Med collecting tools for various types of data, many such 
	  packages can work on the same kind of files. Or the tools depend
	  on each other functionally. For avoiding redundancies and for orchestrating
	  the individual contributors to biomedical workflows in general,
	  we need some extra level. The following projects emerge as such in
          the community.
	</p>
     <hr />
     <table border="0">
    <xsl:for-each select="project">
    <xsl:variable name="url" select="url" />
       <tr>
         <td rowspan="2" align="center" valign="top"><a href="{url}"><xsl:value-of select="@title"/></a></td><td>
		<i><xsl:value-of select="longtitle"/></i>
		<p>
		<xsl:value-of select="description"/>
		</p>
         </td>
           </tr>
           <tr>
             <td>
               <ul>
                  <li>Contributors: <xsl:value-of select="members"/></li>
      	          <li>URL: <a href="{url}"><xsl:value-of select="url"/></a></li>
	        </ul>
              </td>
            </tr>
            <tr><td colspan="2"><hr /></td></tr>
    </xsl:for-each>
          </table>
    </xsl:for-each>


        </td>
      </tr>
    </table>
  </body>
</html>
</xsl:template>

</xsl:stylesheet>
